package com.booking.tokbox.util;



import org.springframework.context.annotation.Configuration;
import com.opentok.OpenTok;
import com.opentok.Session;
import com.opentok.exception.OpenTokException;


@Configuration
public class TokBoxSettings{
	

	private  String apikey = "45421482"; 



	private  String secretkey = "2878e0f0aba916b0505af858623a947af6224cd3"; 

	
	
	private static OpenTok opentok = null;


	public  String getApikey() {
		return apikey;
	}


	public  void setApikey(String api) {
		apikey = api;
	}


	public  String getSecretkey() {
		return secretkey;
	}


	public  void setSecretkey(String secret) {
		secretkey = secret;
	}
	
	
	public void buildOpenTok(){
		if(opentok == null)
    		opentok = new OpenTok(Integer.parseInt(apikey), secretkey);
	}
	
	
	public static Session createVideoSession() throws OpenTokException{
		return opentok!= null ? opentok.createSession() : null;
	}
	
	 /**
     * Generates the client token using the session id
     * @return the token for publishers / subscribers
     */
    public static String createToken(String sessionId){
    	  String token = null;
        try {
            token = opentok.generateToken(sessionId);
        } catch (OpenTokException e) {
            e.printStackTrace();
        }
    	return token;
    }





}
