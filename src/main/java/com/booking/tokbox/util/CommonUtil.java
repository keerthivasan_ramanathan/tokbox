package com.booking.tokbox.util;

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.util.TimeZone;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.exception.JDBCConnectionException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class CommonUtil {
	public static SimpleDateFormat pattern = new SimpleDateFormat("dd/MM/yyyy");



	public static Date getIstDateTime() throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		format.setTimeZone(TimeZone.getTimeZone("IST"));
		Date date = new Date();
		SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateParser.parse(format.format(date));
	}
	
	
	
	public static String getDatein12hrFormat(Date date){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		return dateFormat.format(date);
	}
	
	
	public static String formatEventTime(Date date){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String eventFormattedTime = format.format(date);
		eventFormattedTime = eventFormattedTime.replace(' ','T');
		return eventFormattedTime;
	}
	
	
	public static int getDayOfWeekFromDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.DAY_OF_WEEK);
	}
	
	
	public static Object getEntity(Session session,Class className,int id){
		return session.get(className, id);
	}

	
	public static int getRandom() {
		return 1000 + new Random().nextInt(9000);
	}

	public static Session getSession(SessionFactory sessionFactory) {
		Session session;
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException e) {
			session = sessionFactory.openSession();
		}
		return session;
	}

	public static <T> T parseJsonToObject(Object jsonStr, Class<T> clazz)
			throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		return mapper.convertValue(jsonStr, clazz);
	}

	public static String sendMsg(String msg, String phone) throws IOException {
		/*
		 * String url = null;
		 * 
		 * URL obj = new URL(url); HttpURLConnection con = (HttpURLConnection)
		 * obj.openConnection();
		 * 
		 * BufferedReader in = new BufferedReader(new InputStreamReader(
		 * con.getInputStream())); String inputLine; String response = "";
		 * 
		 * while ((inputLine = in.readLine()) != null) { response += inputLine;
		 * } in.close();
		 */
		return "Success";
	}
	
	
	
}