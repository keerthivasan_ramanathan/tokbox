package com.booking.tokbox.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="availability")
public class Availability implements Serializable{
	
	@Id
	@Column(name="userid")
	private Integer userid;
	
	@Id
	@Column(name="dayid")
	private Integer dayid;
	
	@Id
	@Column(name="starttime")
	private String starttime;
	
	@Id
	@Column(name="created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Integer getDayid() {
		return dayid;
	}

	public void setDayid(Integer dayid) {
		this.dayid = dayid;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}
	
	
	

}
