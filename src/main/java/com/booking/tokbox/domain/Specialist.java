package com.booking.tokbox.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "SPECIALISTS")
public class Specialist {
	
	
	public Specialist() {
		
	}
	
	 @Id
	 @Column(name="userid", unique=true, nullable=false)
	private Integer userid;	
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="speciality",insertable = false, updatable = false)
	private Speciality speciality;
	
	@Column(name="medregno")
	private String regno;
	
	@Column(name="refcode")
	private String referalcode;
	
	
	@OneToOne
    @PrimaryKeyJoinColumn
    private User user;

    
	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public Speciality getSpeciality() {
		return speciality;
	}

	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}

	public String getRegno() {
		return regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}

	public String getReferalcode() {
		return referalcode;
	}

	public void setReferalcode(String referalcode) {
		this.referalcode = referalcode;
	}

	
	
}
