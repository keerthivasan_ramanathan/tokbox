package com.booking.tokbox.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="users")
public class User {
	
	
	public User(){
		
	}
	
	@Id@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="userid")
	private Integer userid = 0;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "mobile")
	private String mobile;
	
	@Column(name = "password")
	private String password;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "type")
	private String userType;
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="cityid",insertable = false, updatable = false)
	private City city;
	
	private String cityid;
	
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="specialityid",insertable = false, updatable = false)
	private Speciality speciality;
	
	private Integer specialityid;
	

	@Column(name="medregno")
	private String regno;
	
	@Column(name="refcode")
	private String referalcode;
	
	@Column(name="availstart")
	private Date availstart;
	
	@Column(name="availend")
	private Date availend;
	
	public String getRegno() {
		return regno;
	}

	public void setRegno(String regno) {
		this.regno = regno;
	}

	public String getReferalcode() {
		return referalcode;
	}

	public void setReferalcode(String referalcode) {
		this.referalcode = referalcode;
	}

	@Column(name = "gender")
	private String gender;
	
	@Column(name = "active")
	private boolean active;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "updated")
	private Date updated;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created")
	private Date created;
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userid != other.userid)
			return false;
		return true;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}
	

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}


	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getCityid() {
		return cityid;
	}

	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

	public Speciality getSpeciality() {
		return speciality;
	}

	public void setSpeciality(Speciality speciality) {
		this.speciality = speciality;
	}

	public Integer getSpecialityid() {
		return specialityid;
	}

	public void setSpecialityid(Integer specialityid) {
		this.specialityid = specialityid;
	}

	public Date getAvailstart() {
		return availstart;
	}

	public void setAvailstart(Date availstart) {
		this.availstart = availstart;
	}

	public Date getAvailend() {
		return availend;
	}

	public void setAvailend(Date availend) {
		this.availend = availend;
	}
	
	
	

}
