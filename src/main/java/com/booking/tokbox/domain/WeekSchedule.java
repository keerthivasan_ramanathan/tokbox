package com.booking.tokbox.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;



public class WeekSchedule {
	
	
	private String suntimings;
	
	private String montimings;
	
	private String tuetimings;
	
	private String wedtimings;
	
	private String thutimings;
	
	private String fritimings;
	
	private String sattimings;
	
	private String startdateStr;
	
	private String enddateStr;
	
	private Date startDate;
	
	private Date endDate;
	
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
	
	
	private HashMap<Integer,List<String>> weeksessions = null;

	public String getSuntimings() {
		return suntimings;
	}

	public void setSuntimings(String suntimings) {
		this.suntimings = suntimings;
	}

	public String getMontimings() {
		return montimings;
	}

	public void setMontimings(String montimings) {
		this.montimings = montimings;
	}

	public String getTuetimings() {
		return tuetimings;
	}

	public void setTuetimings(String tuetimings) {
		this.tuetimings = tuetimings;
	}

	public String getWedtimings() {
		return wedtimings;
	}

	public void setWedtimings(String wedtimings) {
		this.wedtimings = wedtimings;
	}

	public String getThutimings() {
		return thutimings;
	}

	public void setThutimings(String thutimings) {
		this.thutimings = thutimings;
	}

	public String getFritimings() {
		return fritimings;
	}

	public void setFritimings(String fritimings) {
		this.fritimings = fritimings;
	}

	public String getSattimings() {
		return sattimings;
	}

	public void setSattimings(String sattimings) {
		this.sattimings = sattimings;
	}
	
	
	public void prepareWeekSessions(){
		weeksessions = new HashMap<Integer,List<String>>();
		String[] timings = null;
		if(suntimings != null && suntimings.length() > 0)
			setSession(1, suntimings.split(","));
		if(montimings != null && montimings.length() > 0 )
			setSession(2,montimings.split(","));
		if(tuetimings != null && tuetimings.length() > 0)
			setSession(3,tuetimings.split(","));
		if(wedtimings != null && wedtimings.length() > 0)
			setSession(4,wedtimings.split(","));
		if(thutimings != null && thutimings.length() > 0)
			setSession(5, thutimings.split(","));
		if(fritimings != null && fritimings.length() > 0)
			setSession(6, fritimings.split(","));
		if(sattimings != null && sattimings.length() > 0)
			setSession(7,sattimings.split(","));
	}
	
	
	private void setSession(int day,String[] sessions){
	   List<String> sessionList =  Arrays.asList(sessions);
		weeksessions.put(day,sessionList);
	}

	public HashMap<Integer, List<String>> getWeeksessions() {
		return weeksessions;
	}

	public void setWeeksessions(HashMap<Integer, List<String>> weeksessions) {
		this.weeksessions = weeksessions;
	}

	public String getStartdateStr() {
		return startdateStr;
	}

	public void setStartdateStr(String startdateStr) {
		this.startdateStr = startdateStr;
	}

	public String getEnddateStr() {
		return enddateStr;
	}

	public void setEnddateStr(String enddateStr) {
		this.enddateStr = enddateStr;
	}

	public Date getStartDate() throws ParseException {
		if(startdateStr!=null){
			return dateFormatter.parse(startdateStr);
		}
		return null;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
		if(this.startDate!=null)
		this.startdateStr = dateFormatter.format(this.startDate);
	}

	public Date getEndDate() throws ParseException {
		if(enddateStr!=null){
			return dateFormatter.parse(enddateStr);
		}
		return null;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
		if(this.endDate!=null)
		this.enddateStr = dateFormatter.format(this.endDate);
	}
	
	
	
}
