package com.booking.tokbox.domain;



import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;


@Entity
@Table(name = "BOOKING")
public class Booking implements Serializable{
	
	public Booking(){
		
	}
	
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "title")
	private String title;	
	
	@Column(name = "descr")
	private String desc;
	

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "slotid",insertable = false, updatable = false)
	private Slot slot;
	
	private Integer slotid;
	
	private Integer subscriberid;
	

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "subscriberid",insertable = false, updatable = false)
	private User subscriber;
	
	@Column(name="toksession")
	private String toksession;
	
	@Column(name="pubtkn")
	private String pubtkn;
	
	@Column(name="subtkn")
	private String subtkn;
	
	@Column(name="expiry")
	private Long expiry;

	@Column(name = "created")
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "slotid",referencedColumnName="slotid")
	public Slot getSlot() {
		return slot;
	}

	public void setSlot(Slot slot) {
		this.slot = slot;
	}
	
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name = "subscriberid",referencedColumnName="userid")
	public User getSubscriber() {
		return subscriber;
	}

	public void setSubscriber(User subscriber) {
		this.subscriber = subscriber;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Integer getSlotid() {
		return slotid;
	}

	public void setSlotid(Integer slotid) {
		this.slotid = slotid;
	}

	public Integer getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(Integer subscriberid) {
		this.subscriberid = subscriberid;
	}

	public String getPubtkn() {
		return pubtkn;
	}

	public void setPubtkn(String pubtkn) {
		this.pubtkn = pubtkn;
	}

	public String getSubtkn() {
		return subtkn;
	}

	public void setSubtkn(String subtkn) {
		this.subtkn = subtkn;
	}

	public Long getExpiry() {
		return expiry;
	}

	public void setExpiry(Long expiry) {
		this.expiry = expiry;
	}

	public String getToksession() {
		return toksession;
	}

	public void setToksession(String toksession) {
		this.toksession = toksession;
	}
	
	
	
}
