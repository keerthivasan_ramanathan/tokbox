package com.booking.tokbox.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.booking.tokbox.dao.BookingDao;
import com.booking.tokbox.dao.SlotDao;
import com.booking.tokbox.dao.UserDao;
import com.booking.tokbox.domain.Booking;
import com.booking.tokbox.domain.Event;
import com.booking.tokbox.domain.Slot;
import com.booking.tokbox.domain.User;
import com.booking.tokbox.util.CommonUtil;
import com.booking.tokbox.util.SlotUtil;

@Controller
public class BookingController {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Autowired
	private BookingDao bookingDao;
	
	
	@Autowired
	private UserDao userDao;
	
	
	@Autowired
	private SlotDao slotDao;
	
	
	
	@RequestMapping(value="/bookings",method=RequestMethod.GET)
	public ModelAndView home(@RequestParam(value="start",defaultValue="0",required=false) int start,
			@RequestParam(value="range",defaultValue="10",required=false) int range,HttpServletRequest request){
		ModelAndView mav = new ModelAndView("bookings");
		HttpSession httpSession = request.getSession(false);
		List<Booking> bookings = null;
		int pagenumber = 0;
		if(httpSession != null){
			User user = (User) request.getSession().getAttribute("user");
			Session session = CommonUtil.getSession(sessionFactory);
			if(user.getUserType().equals("U")){
				bookings = bookingDao.fetchBookingsBySubscriber(user, session,start, range);
				mav.addObject("viewtype","userview");
				pagenumber = bookingDao.getPageCount(session,1,user);
			}
			else{
				bookings = bookingDao.fetchBookingsByOwner(user, session,start, range);
				mav.addObject("viewtype","docview");
				pagenumber = bookingDao.getPageCount(session,2,user);
			}
			mav.addObject("pageno",pagenumber);
			mav.addObject("currentpage",(start/10)+1);
			mav.addObject("bookings",bookings);
			httpSession.setAttribute("bookings", bookings);
		}
		return mav;
	}
	
    
	
	@ResponseBody
	@RequestMapping(value="/api/fetchbookings",method=RequestMethod.GET)
	public List<Event> getEventBookings(HttpServletRequest request){
		HttpSession httpSession = request.getSession(false);
		if(httpSession != null){
			User user = (User) request.getSession().getAttribute("user");
			Session session = CommonUtil.getSession(sessionFactory);
			List<Booking> bookings = bookingDao.fetchBookings(user, session);
			if(bookings == null || bookings.size() == 0)
				return null;
			
			return SlotUtil.createEventsForBookings(bookings);
		}
		return null;
	}
	
	
	@RequestMapping(value="/viewcalendar",method=RequestMethod.GET)
	public ModelAndView viewCalendar(@RequestParam("uid") int ownerid,HttpServletRequest request){
		HttpSession httpSession = request.getSession(false);
		if(httpSession != null){
		User user = (User) request.getSession().getAttribute("user");
		ModelAndView mav  = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User owner = (User)CommonUtil.getEntity(session, User.class,ownerid);
		mav.addObject("owner",owner);
		mav.addObject("user",user);
		mav.setViewName("viewcalendar");
		return mav;
		}
		return null;
	}
	
	
	@RequestMapping(value="/addbookinginfo",method=RequestMethod.POST)
	public ModelAndView addBooking(@RequestParam("title") String title,
			@RequestParam("desc") String desc,@RequestParam("slotid") int slotid,@RequestParam("subscriberid") int subscriberid,HttpServletRequest request){
		HttpSession httpSession = request.getSession(false);
		if(httpSession != null){
		User user = (User) request.getSession().getAttribute("user");
		ModelAndView mav  = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		Booking booking = bookingDao.addBooking(title, desc, slotid, subscriberid, session);
		if(booking!=null){
			Query query = session.createQuery("Update Slot set status =? where id = ?").setParameter(0,"B")
					.setParameter(1,slotid);
			query.executeUpdate();
			mav.addObject("msgtitle","Booking Successful!");
			mav.addObject("msg","Your appointment has been confirmed with "+booking.getSlot().getUser().getName()+".Please"
					+ " join the session at "+CommonUtil.getDatein12hrFormat(booking.getSlot().getStartdate()));
		}else{
			mav.addObject("errortitle","Booking got Failed!");
			mav.addObject("error","Something went wrong while we were booking your session. we regret for the"
					+ "inconvenience caused");
		}
		mav.addObject("pageno",1);
		mav.addObject("currentpage",1);
		mav.addObject("user",user);
		List<Booking> bookings = bookingDao.fetchBookings(user, session);
		mav.addObject("bookings", bookings);
		mav.setViewName("bookings");
		return mav;
		}
		return null;
	}
	
	
	
	@ResponseBody
	@RequestMapping(value="/api/fetchavailability",method=RequestMethod.GET)
	public List<Event> getAvailability(@RequestParam("uid") int owner,@RequestParam("start") String start,
			@RequestParam("end") String end,HttpServletRequest request) throws ParseException{
		HttpSession httpSession = request.getSession(false);
		if(httpSession != null){
			Session session = CommonUtil.getSession(sessionFactory);
			List<Slot> slots = slotDao.fetchSlots(session,owner,start,end);
			if(slots == null || slots.size() == 0)
				return null;
			return SlotUtil.createEventsForSlots(slots);
		}
		return null;
	}
	
	
	public static void main(String[] args){
		Date date = new Date();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time = format.format(date);
		time = time.replace(' ','T');
		System.out.println(time);
	}

}
