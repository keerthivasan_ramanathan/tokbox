package com.booking.tokbox.controllers;

import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import com.booking.tokbox.dao.BookingDao;
import com.booking.tokbox.domain.Booking;
import com.booking.tokbox.domain.User;
import com.booking.tokbox.util.CommonUtil;
import com.booking.tokbox.util.TokBoxSettings;
import com.opentok.Role;
import com.opentok.TokenOptions;
import com.opentok.exception.OpenTokException;

@Controller
public class TokBoxController {

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private BookingDao bookingDao;

	@RequestMapping(value = "/joinsession", method = RequestMethod.GET)
	public ModelAndView home(@RequestParam("id") int bookingid,
			HttpServletRequest request) throws OpenTokException {
		ModelAndView mav = new ModelAndView();
		HttpSession httpSession = request.getSession(false);
		Booking booking = null;
		Session session = CommonUtil.getSession(sessionFactory);
		Query query = null;
		String sessionid = null;
		if (httpSession != null) {
			booking = (Booking) session.get(Booking.class, bookingid);
				Date currentDate = new Date();
				if (booking.getSlot().getStartdate().before(currentDate)
						&& booking.getSlot().getEnddate().after(currentDate)) {
					User user = (User) httpSession.getAttribute("user");
					TokBoxSettings tokboxSettings = new TokBoxSettings();
					tokboxSettings.buildOpenTok();
					mav.addObject("apikey",tokboxSettings.getApikey());
					if(booking.getToksession() == null){
					sessionid = TokBoxSettings.createVideoSession().getSessionId();
					query = session.createQuery("Update Booking set toksession = ? where id = ?")
							.setParameter(0, sessionid).setParameter(1,bookingid);
					query.executeUpdate();
					}else{
						sessionid = booking.getToksession();
					}
					mav.addObject("sessionid",sessionid);
					 // Generate a token. Use the Role value appropriate for the user.
					Long expiry = booking.getSlot().getEnddate().getTime()/1000;
					query = session.createQuery("Update Booking set expiry = ? where id = ?")
							.setParameter(0, expiry).setParameter(1,bookingid);
					query.executeUpdate();
			        TokenOptions tokenOpts = new TokenOptions.Builder()
			          .role(Role.MODERATOR)
			          .expireTime(expiry) // in one week
			          .data(user.getUserid()+":"+user.getName()+":"+user.getCity())
			          .build();
			        String token = TokBoxSettings.createToken(sessionid);
				
					if (user.getUserType().equals("U")) {
						query = session.createQuery("Update Booking set subtkn = ? where id = ?")
								.setParameter(0, token).setParameter(1,bookingid);
						query.executeUpdate();
					if (booking.getPubtkn() == null) {
						mav.addObject("msgtitle","Patience please..");
						mav.addObject("msg","Specialist hasn't joined the session.Please wait for sometime");	
					} else {
						
					}
				}else{
					query = session.createQuery("Update Booking set pubtkn = ? where id = ?")
							.setParameter(0, token).setParameter(1,bookingid);
					query.executeUpdate();
					if (booking.getSubtkn() == null) {
						mav.addObject("msgtitle","Patience please..");
						mav.addObject("msg","Subscriber hasn't joined the session.Please wait for sometime");
						
					}
				}
				mav.addObject("token",token);
				mav.setViewName("Tokboxcall");
			} else {
				mav.addObject("errortitle","Invalid session");
				mav.addObject("error","This session is not currently happening");
				mav.setViewName("bookings");
			}
		}
		return mav;
	}

}
