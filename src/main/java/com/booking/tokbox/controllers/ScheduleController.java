package com.booking.tokbox.controllers;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.booking.tokbox.dao.AvailabilityDao;
import com.booking.tokbox.dao.SlotDao;
import com.booking.tokbox.dao.SpecialistDao;
import com.booking.tokbox.dao.SpecialityDao;
import com.booking.tokbox.domain.Speciality;
import com.booking.tokbox.domain.User;
import com.booking.tokbox.domain.WeekSchedule;
import com.booking.tokbox.util.CommonUtil;


@Controller
public class ScheduleController {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@Autowired
	private SpecialistDao specialistDao;
	
	
	@Autowired
	private SpecialityDao specialityDao;
	
	
	@Autowired
	private SlotDao slotDao;
	
	@Autowired
	private AvailabilityDao availabilityDao;
	
	@RequestMapping(value="/searchspecialists",method=RequestMethod.POST)
	public ModelAndView searchSpecialits(@ModelAttribute("speciality") Speciality speciality,@RequestParam(value="start",defaultValue="0",required=false) int start,
			@RequestParam(value="range",defaultValue="10",required=false) int range){
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		List<User> specialists = specialistDao.fetchSpecialists(session, speciality.getId(), start, range);
		int pagenumber = specialistDao.getPageCount(session, speciality.getId());
		Map specialityValues = specialityDao.fetchSpecialities();
		mav.addObject("specialities",specialityValues);
		mav.addObject("specialists",specialists);
		mav.addObject("pageno",pagenumber);
		mav.addObject("currentpage",(start/10)+1);
		mav.setViewName("specialists");
		return mav;
	}
	
	@RequestMapping(value="/specialists",method=RequestMethod.GET)
	public ModelAndView loadSpecialists(@ModelAttribute("speciality") Speciality speciality
			,@RequestParam(value="start",defaultValue="0",required=false) int start,
			@RequestParam(value="range",defaultValue="10",required=false) int range){
		ModelAndView mav = new ModelAndView();
		Map specialityValues = specialityDao.fetchSpecialities();
		Session session = CommonUtil.getSession(sessionFactory);
		List<User> specialists = specialistDao.fetchSpecialists(session, -1, start, range);
		int pagenumber = specialistDao.getPageCount(session,-1);
		mav.addObject("specialities",specialityValues);
		mav.addObject("specialists",specialists);
		mav.addObject("pageno",pagenumber);
		mav.addObject("currentpage",(start/10)+1);
	    mav.setViewName("specialists");
	    return mav;
	}
	
	
	@RequestMapping(value="/availability",method=RequestMethod.GET)
	public ModelAndView showAvailability(@ModelAttribute("schedule") WeekSchedule weekSchedule,HttpServletRequest request){
		ModelAndView mav = new ModelAndView();
		HttpSession httpSession = request.getSession(false);
		if(httpSession!=null){
			User user = (User)httpSession.getAttribute("user");
			Session session = CommonUtil.getSession(sessionFactory);
			mav.addObject("owner",user);
			weekSchedule = availabilityDao.getWeekSchedule(user.getUserid(), session);
			mav.addObject("schedule",weekSchedule);
			mav.setViewName("availability");
		}
		return mav;
	}
	
	
	@RequestMapping(value="/saveavailability", method=RequestMethod.POST)
	public ModelAndView saveAvailability(@ModelAttribute("schedule") WeekSchedule weekSchedule,HttpServletRequest request) throws ParseException{
		ModelAndView mav = new ModelAndView();
		User user = null;
		HttpSession httpSession = request.getSession(false);
		if(httpSession!=null){
			user = (User) httpSession.getAttribute("user");
		}
		Session session = CommonUtil.getSession(sessionFactory);
		availabilityDao.updateAvailabilityPeriod(user.getUserid(), session,weekSchedule.getStartDate(),weekSchedule.getEndDate());
		Query q = session.createQuery("delete Availability where userid = ?").setInteger(0, user.getUserid());
		q.executeUpdate();
		weekSchedule.prepareWeekSessions();
		availabilityDao.generateAvailability(user.getUserid(),weekSchedule.getStartDate(),weekSchedule.getEndDate(),
				weekSchedule.getWeeksessions(), session);
		mav.addObject("schedule",weekSchedule);
		mav.setViewName("availability");
		return mav;
	}
	
	
	
	
	
	

}
