package com.booking.tokbox.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.booking.tokbox.dao.BookingDao;
import com.booking.tokbox.dao.CityDao;
import com.booking.tokbox.dao.SlotDao;
import com.booking.tokbox.dao.SpecialityDao;
import com.booking.tokbox.dao.UserDao;
import com.booking.tokbox.domain.Booking;
import com.booking.tokbox.domain.City;
import com.booking.tokbox.domain.User;
import com.booking.tokbox.util.CommonUtil;
import com.booking.tokbox.util.SlotUtil;


@SessionAttributes("user")
@Controller
public class UserController {
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	CityDao cityDao;
	
	@Autowired
	BookingDao bookingDao;
	
	
	@Autowired
	SpecialityDao specialityDao;
	
	@Autowired
	SlotDao slotDao;
		
	@Autowired
	private SessionFactory sessionFactory;
	
	
	@RequestMapping(value={"/", "/home"},method=RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView mav = new ModelAndView("home");
		return mav;
	}
	
	
	
	
	
	@RequestMapping(value="/main",method=RequestMethod.GET)
	public ModelAndView loadHome(HttpServletRequest request) throws Exception{
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User user = userDao.findByUserName(request.getUserPrincipal().getName(), session);
		request.getSession().setAttribute("user",user);
		List<Booking> bookings = bookingDao.fetchBookings(user, session);
		if(bookings!=null)
		mav.addObject("bookings_count",bookings.size());
		else
		mav.addObject("bookings_count",0);
		mav.addObject("user",user);
	    mav.setViewName("main");
		return mav;
	}
	
	@RequestMapping(value="/profile",method=RequestMethod.GET)
	public ModelAndView profile(HttpServletRequest request) throws Exception{
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User user = userDao.findByUserName(request.getUserPrincipal().getName(), session);
		mav.addObject("user",user);
		Map cityValues = cityDao.fetchCities();
		mav.addObject("cities",cityValues);
		Map specialityValues = specialityDao.fetchSpecialities();
		mav.addObject("specialities",specialityValues);
		mav.setViewName("profile");
		return mav;
	}
	
	
	@RequestMapping(value="/register",method=RequestMethod.GET)
	public ModelAndView userregister(){
		ModelAndView mav = new ModelAndView("userregister", "user", new User());
		Map cityValues = cityDao.fetchCities();
		mav.addObject("cities",cityValues);
		return mav;
	}
	
	@RequestMapping(value="/spec_register",method=RequestMethod.GET)
	public ModelAndView specialistRegistration(){
		ModelAndView mav = new ModelAndView("userregister", "user", new User());
		Map cityValues = cityDao.fetchCities();
		mav.addObject("cities",cityValues);
		Map specialityValues = specialityDao.fetchSpecialities();
		mav.addObject("specialities",specialityValues);
		return mav;
	}
	
	
	@RequestMapping(value="/saveprofile",method=RequestMethod.POST)
	public ModelAndView profile(@ModelAttribute("user") User user) throws Exception{
		ModelAndView mav = new ModelAndView();
		Session session = CommonUtil.getSession(sessionFactory);
		User persistedUser = userDao.prepareUpdatedEntity(user);
		boolean saved = userDao.saveUser(session,persistedUser);
		  if (saved)
			  mav.addObject("success", "Profile saved successfully");
			else
				mav.addObject("error", "OOh,We are Sorry.Something went wrong!");
		mav.addObject("user",user);
		Map cityValues = cityDao.fetchCities();
		mav.addObject("cities",cityValues);
		Map specialityValues = specialityDao.fetchSpecialities();
		mav.addObject("specialities",specialityValues);
		mav.setViewName("profile");
		return mav;
	}
	
	
	
	
	@RequestMapping(value="/adduser",method=RequestMethod.POST)
	public ModelAndView adduser(@ModelAttribute("user") User user){
		  ModelAndView mav = new ModelAndView("userregister");
		  boolean added = false;
		  user.setUpdated(new Date());
		  if(user.getRegno() == null)
		  user.setUserType("U");
		  else
	      user.setUserType("S");	  
		  Session session = CommonUtil.getSession(sessionFactory);
		  try {
			  added = userDao.addUserDetail(user, session);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  if (added)
			  mav.addObject("msg", "You have successfully registered.");
			else
				mav.addObject("error", "OOh,We are Sorry.Something went wrong!");
		  mav.addObject("user", new User());
		  return mav;
	}
	
	
	@RequestMapping(value="/termsAndConditions",method=RequestMethod.GET)
	public ModelAndView termsAndConditions(){
		ModelAndView mav = new ModelAndView("termsandconditions");
		return mav;
	}
	
	@RequestMapping(value="/contactUs",method=RequestMethod.GET)
	public ModelAndView contactus(){
		ModelAndView mav = new ModelAndView("contactus");
		return mav;
	}

}
