package com.booking.tokbox.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.TemporalType;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;

import com.booking.tokbox.domain.Booking;
import com.booking.tokbox.domain.Event;
import com.booking.tokbox.domain.Slot;
import com.booking.tokbox.util.CommonUtil;

public class SlotDao {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<Slot> fetchSlots(Session session,int ownerid,String start,String end) throws ParseException{
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date startDate = dateFormat.parse(start);
		Date endDate = dateFormat.parse(end);
		List<Slot> slots = null;
		Query query = session.createQuery("From Slot where ownerid = ? and startdate > ? and enddate < ?").setParameter(0,ownerid)
				.setParameter(1, startDate).setParameter(2,endDate);
		slots = query.list();
		if(slots.size() > 0)
			return slots;
		return null;
	}
	
	
	
	public void generateSlots(Date startDate,Date endDate,String startTimeStr,int ownerid,Session session) throws ParseException{
		Date startDateClone = new Date(startDate.getTime());
		SimpleDateFormat dateFormatonly = new SimpleDateFormat("yyyy-MM-dd");
		String startDateWithoutTimeStr = dateFormatonly.format(startDateClone);
		SimpleDateFormat dateFormatWithTime = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		String sessionStartDateWithTimeStr = startDateWithoutTimeStr+" "+startTimeStr;
		Date sessionStartDateWithTime = dateFormatWithTime.parse(sessionStartDateWithTimeStr);
		List<Slot> slots = new ArrayList<Slot>();
		while(sessionStartDateWithTime.after(startDate) && sessionStartDateWithTime.before(endDate)){
			Slot slot = new Slot();
			slot.setOwnerid(ownerid);
			slot.setStartdate(sessionStartDateWithTime);
			DateTime jdStartTime = new DateTime(sessionStartDateWithTime);
			DateTimeFormatter dtfOut = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
			System.out.println(dtfOut.print(jdStartTime));
			DateTime jdEndTime = jdStartTime.plusMinutes(30);
			slot.setEnddate(jdEndTime.toDate());
			slot.setStatus("A");
			System.out.println(dtfOut.print(jdEndTime));
			sessionStartDateWithTime = jdStartTime.plusWeeks(1).toDate();
			slots.add(slot);
			}
		Transaction tx = session.beginTransaction();
		for(int i=0;i<slots.size();i++){
			session.save(slots.get(i));
		    if ( i % 20 == 0 ) { //20, same as the JDBC batch size
		        //flush a batch of inserts and release memory:
		        session.flush();
		        session.clear();
		    }
		}
		tx.commit();
		
	}
	

	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
