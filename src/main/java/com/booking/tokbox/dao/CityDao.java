package com.booking.tokbox.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.booking.tokbox.domain.City;
import com.booking.tokbox.util.CommonUtil;

@Component
public class CityDao {
	
	@Autowired
	SessionFactory sessionFactory;
	
	
	@SuppressWarnings("unchecked")
	public Map<String,String> fetchCities(){
		@SuppressWarnings("rawtypes")
		List objects = selectAll(City.class);
		List<City> cities = (List<City>) (List<?>) objects;
		Map<String,String> cityMap = new LinkedHashMap<String,String>();
		for(City city : cities){
			cityMap.put(city.getId()+"",city.getName());
		}
		return cityMap;
	}
	
	
	@SuppressWarnings("rawtypes")
	List selectAll(Class clazz) {
		Session session = CommonUtil.getSession(sessionFactory);
	    return session.createCriteria(clazz).list();
	}


	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	
	

}
