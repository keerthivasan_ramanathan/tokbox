package com.booking.tokbox.dao;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.booking.tokbox.domain.Specialist;
import com.booking.tokbox.domain.Speciality;
import com.booking.tokbox.domain.User;
import com.booking.tokbox.util.CommonUtil;


@Component
public class UserDao {
	
	@Autowired
	SessionFactory sessionFactory;
	
	
	

	public boolean addUserDetail(User user, Session session)
			throws Exception {
		Serializable check = null;
		boolean userAdded = false;
		user.setUpdated(new Timestamp(new Date().getTime()));
		user.setActive(true);
		if (user.getUserid() == 0) {
			if (user.getPassword() != null)
				user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
			check = session.save(user);
		
			
			if (Integer.valueOf(check.toString()) > 0)
				userAdded = true;
			return userAdded;
		} else {
			session.saveOrUpdate(user);
			session.flush();
		}
		return false;
	}
	
	
	public User prepareUpdatedEntity(User user){
		Session session = CommonUtil.getSession(sessionFactory);
		User persistedUser = (User) CommonUtil.getEntity(session, User.class, user.getUserid());
		persistedUser.setName(user.getName());
		persistedUser.setEmail(user.getEmail());
		persistedUser.setMobile(user.getMobile());
		persistedUser.setSpecialityid(user.getSpecialityid());
		persistedUser.setRegno(user.getRegno());
		persistedUser.setReferalcode(user.getReferalcode());
		persistedUser.setGender(user.getGender());
		persistedUser.setCityid(user.getCityid());
		persistedUser.setUpdated(new Date());
		return persistedUser;
	}
	
	
	public boolean saveUser(Session session,User user){
		boolean updated = false;
		session.merge(user);
		session.flush();
		updated = true;
		return updated;
	}
	
	
	@SuppressWarnings("unchecked")
	public User findByUserName(String username, Session session)
			throws Exception {
		if (session == null)
			session = com.booking.tokbox.util.CommonUtil.getSession(sessionFactory);
		List<User> users = new ArrayList<User>();
		users = session.createQuery("from User where email=?")
				.setParameter(0, username).list();
		if (users.size() > 0) {
			return users.get(0);
		} else {
			return null;
		}

	}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	

}
