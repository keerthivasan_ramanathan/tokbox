package com.booking.tokbox.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.booking.tokbox.domain.Booking;
import com.booking.tokbox.domain.Slot;
import com.booking.tokbox.domain.User;


@Component
public class BookingDao {
	
	
	@Autowired
	SessionFactory sessionFactory;
		
	public List<Booking> fetchBookingsBySubscriber(User user,Session session,int start,int range){
		List<Booking> bookings = new ArrayList<Booking>();
		Query query = session.createQuery("from Booking where subscriber=?")
				.setParameter(0, user);
		query.setFirstResult(start);
		query.setMaxResults(range);
		bookings = query.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}
	
	
	
	public List<Booking> fetchBookingsByOwner(User user,Session session,int start,int range){
		List<Booking> bookings = new ArrayList<Booking>();
		Query query = session.createQuery("from Booking where slot.user=?")
				.setParameter(0, user);
		query.setFirstResult(start);
		query.setMaxResults(range);
		bookings = query.list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}
	
	/**
	 * ajax call api data service - not used now
	 * @param user
	 * @param session
	 * @return
	 */
	public List<Booking> fetchBookings(User user,Session session){
		List<Booking> bookings = new ArrayList<Booking>();
		bookings = session.createQuery("from Booking where subscriber=?")
				.setParameter(0, user).list();
		if (bookings.size() > 0) {
			return bookings;
		} else {
			return null;
		}
	}
	
	public Booking addBooking(String title,String desc,int slotid,int subscriberid,Session session){
		Booking booking = new Booking();
		booking.setTitle(title);
		booking.setDesc(desc);
		booking.setSlotid(slotid);
		booking.setSubscriberid(subscriberid);
		booking.setCreated(new Date());
		Integer bookingid = (Integer) session.save(booking);
		session.flush();
		session.refresh(booking);
		Booking bookingEntity = (Booking) session.get(Booking.class, bookingid);
		return bookingEntity;
	}
	
	
	public int getPageCount(Session session,int type,User user){
		List<Booking> bookings = null;
		int pagenumber = 0;
		Query query = null;
		if(type == 1){
			query = session.createQuery("from Booking where subscriber=?").setParameter(0, user);
			bookings=query.list();
		}else{
			query = session.createQuery("from Booking where slot.user=?")
					.setParameter(0, user);
			bookings=query.list();
		}
		if(bookings.size() % 10 == 0)
			pagenumber = bookings.size() / 10;
		else
			pagenumber = (bookings.size() / 10)+1;
		if(pagenumber == 0)
			pagenumber = 1;
		return pagenumber;
	}
	

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
