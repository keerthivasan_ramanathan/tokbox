package com.booking.tokbox.dao;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.booking.tokbox.domain.City;
import com.booking.tokbox.domain.Speciality;
import com.booking.tokbox.util.CommonUtil;

@Component
public class SpecialityDao {
	
	
	@Autowired
	SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	@SuppressWarnings("rawtypes")
	List selectAll(Class clazz) {
		Session session = CommonUtil.getSession(sessionFactory);
	    return session.createCriteria(clazz).list();
	}
	
	
	@SuppressWarnings("unchecked")
	public Map<String,String> fetchSpecialities(){
		@SuppressWarnings("rawtypes")
		List objects = selectAll(Speciality.class);
		List<Speciality> specialities = (List<Speciality>) (List<?>) objects;
		Map<String,String> specialityMap = new LinkedHashMap<String,String>();
		for(Speciality speciality : specialities){
			specialityMap.put(speciality.getId()+"",speciality.getName());
		}
		return specialityMap;
	}

	
	
	

}
