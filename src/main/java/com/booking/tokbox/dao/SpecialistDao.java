package com.booking.tokbox.dao;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.booking.tokbox.domain.User;

import java.util.List;

@Component
public class SpecialistDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	
	
	public List<User> fetchSpecialists(Session session,int specialityid,int start,int range){
		List<User> specialists = null;
		Query query = null;
		if(specialityid == -1){
			query = session.createQuery("from User where type=?").setParameter(0, "S");
			query.setFirstResult(start);
			query.setMaxResults(range);
			specialists=query.list();
		}else{
			query = session.createQuery("from User where type=? and specialityid = ?").setParameter(0, "S").setParameter(1, specialityid);
			query.setFirstResult(start);
			query.setMaxResults(range); 
			specialists=query.list();
		}
		if (specialists.size() > 0)
			return specialists;
		return null;
	}
	
	
	public int getPageCount(Session session,int specialityid){
		List<User> specialists = null;
		int pagenumber = 0;
		Query query = null;
		if(specialityid == -1){
			query = session.createQuery("from User where type='S'");
			specialists=query.list();
		}else{
			query = session.createQuery("from User where type='S' and specialityid = ?")
					.setParameter(0, specialityid);
			specialists=query.list();
		}
		if(specialists.size() % 10 == 0)
			pagenumber = specialists.size() / 10;
		else
			pagenumber = (specialists.size() / 10)+1;
		if(pagenumber == 0)
			pagenumber = 1;
		return pagenumber;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	
	

}
