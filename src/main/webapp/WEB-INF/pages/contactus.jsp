<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<title>Your Medics</title>
<link rel="shortcut icon" type="image/x-icon"
	href="images/favicon.ico" /><style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:Consolas;
	panose-1:2 11 6 9 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=EN-US>

<div class=WordSection1>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:28.0pt;font-family:"Georgia","serif";color:black'>Contact Us</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0in;margin-bottom:.0001pt;
text-align:center;line-height:normal;text-autospace:none'><b><span
style='font-size:10.0pt;font-family:"Georgia","serif";color:black'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><img width=134 height=129
src="images/logo.png" align=left hspace=12></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:12.0pt;font-family:"Georgia","serif";
color:black'>Arvene Healthcare pvt ltd., </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:12.0pt;font-family:"Georgia","serif";
color:black'>Quest Offices, 5th Floor,</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:12.0pt;font-family:"Georgia","serif";
color:black'>Technopolis, </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:12.0pt;font-family:"Georgia","serif";
color:black'>Sector 54, Golf course road, </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:12.0pt;font-family:"Georgia","serif";
color:black'>Gurgaon 122002. </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;line-height:
normal;text-autospace:none'><span style='font-size:12.0pt;font-family:"Georgia","serif";
color:black'><a
href="mailto:contactus@yourmedics.in"><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";mso-fareast-font-family:"Arial Unicode MS";
mso-hansi-font-family:"Arial Unicode MS";mso-bidi-font-family:"Arial Unicode MS";
color:#00000A'>contactus@yourmedics.in</span></a></span></p>

</div>

</body>

</html>
