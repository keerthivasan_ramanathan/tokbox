<%@ include file="outerheader.jsp" %>
    <!-- /header -->
	






	<div class="pageContent">
		<div class="headerContent">

			<div class="upperBodyContent">
				<div class="upperBodyContentHeader">YOUR MEDICS</div>
				<div class="upperBodyContentText"></div>
				<div class="signUpButton">Sign Up for Free</div>
			</div>
		</div>

		<div class="bodyContent">
			<div class="whyMedics">Why Your Medics</div>
			<div class="whyMedicsSection">
				<div class="whyMedicsSectionLeft">
					<img src="images/Landing-Page_07-12.png" />
					<div class="description">"Save your medics records on a cloud
						based platform and access anytime, anywhere with a single click !"</div>
				</div>
				<div class="whyMedicsSectionCentre">
					<img src="images/Landing-Page_07.png" />
					<div class="description">"Set Medicine reminders. Allows you
						to add multiple accounts and free sms reminder feature for your
						loved ones."</div>
				</div>
				<div class="whyMedicsSectionRight">
					<img src="images/Landing-Page_07-09.png" />
					<div class="description">"Many more interactive features like
						graphical formats fot the reports and summary of records so that
						your doctor can take a well informed decision !"</div>
				</div>
			</div>
		</div>
		<div class="lowerBodyContent">
			<div class="lowerBodyHead">Testimonials</div>
			<div class="row">
				<div class="left">
					<div class="leftcomment">
						"Now I take my medicines regularly and on time. Thanks to your
						medics app reminders"
						<p>Dr. Randhir Singh Dahiya</p>
						<img class="commentImage" src="images/DrRandhirSingh.png" />
					</div>
				</div>
				<div class="right">
					<div class="rightcomment">
						"The best part with taking your daily dose is that you can
						strictly control your borderline diseases !"
						<p>Amit Wadhwa</p>
						<img class="commentImage" src="images/Amit.png" />
					</div>
				</div>

			</div>
			<div class="row">
				<div class="left">
					<div class="leftcomment">
						"YourMedics helps me remember my medications via SMS reminders.It
						is very helpful for chronic patients-"
						<p>Dr. Dharam Vir Malhotra</p>
						<img class="commentImage" src="images/DrDharamVirSingh.png" />
					</div>

				</div>

				<div class="right">
					<div class="rightcomment">
						"I have saved all the medical records of my parents on yourmedics.
						It helps me track their health better."
						<p>Dheeraj Dhamija</p>

						<img class="commentImage" src="images/dheeraj.png" />
					</div>
				</div>
			</div>
		</div>
		
<div class="footerContent">
			<div class="footerHeader">Download our free mobile app</div>
			<div class="footerImages">
				<a href="#"><img class="footerImage"
					src="images/App-store.png" /></a> <a
					href="https://play.google.com/store/apps/details?id=com.medicine.yourmedics"><img
					class="footerImage" src="images/google-play.png" /></a>
			</div>
			<div class="footerBorder"></div>
			<div class="footerLinks">
				<div class="footerLink">
					<a style="color: white;" href="home">Home</a>
				</div>
				<div class="footerLink" onclick="myFunction()">
					<a style="text-decoration: underline; cursor: pointer;color: white;">Service</a>
				</div>
				<div class="footerLink">
					<a style="color: white;" href="http://www.yourmedics.wordpress.com">BLOG</a>
				</div>
				<div class="footerLink" onclick="contactUs()">
					<a style="text-decoration: underline; cursor: pointer;color: white;">Contact
						Us</a>
				</div>
			</div>
		</div>





</div>
<!--  /Login form -->
<%@ include file="outerfooter.jsp" %>
