<%@ include file="header.jsp"%>
<section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
          <h1>Profile</h1>
        </div>
      </div>
    </div>
  </section>

  <section id="profile-page" class="container">
    <form:form class="center" action='saveprofile' method="POST" modelAttribute="user">
      <fieldset class="registration-form">
        <div class="control-group">
          <!-- Name -->
          <div class="controls">
          <form:input type="hidden" id="userid" name="userid" path="userid" />
            <form:input type="text" id="name" name="name" path="name" placeholder="Name" class="input-xlarge"/>
          </div>
        </div>

        <div class="control-group">
          <!-- E-mail -->
          <div class="controls">
            <form:input type="text" id="email" name="email" path="email" placeholder="E-mail" class="input-xlarge"/>
          </div>
        </div>

       
     <%--    <div class="control-group">
          <!-- Password-->
          <div class="controls">
            <form:input type="password" id="password" name="password" path="password" placeholder="Password" class="input-xlarge"/>
          </div>
        </div>

        <div class="control-group">
          <!-- Password -->
          <div class="controls">
            <input type="password" id="password_confirm" name="password_confirm" placeholder="Password (Confirm)" class="input-xlarge">
          </div>
        </div> --%>
        
         <div class="control-group">
          <!-- Mobile -->
          <div class="controls">
            <form:input length="10" id="mobile" name="mobile" path="mobile" placeholder="Mobile number" class="input-xlarge"/>
         
          </div>
        </div>
        <c:choose>
        <c:when test="${user.userType == 'S'}">
        <div class="control-group">
          <!-- Speciality -->
          <div class="controls">
        <form:select class="input-xlarge" name="speciality" path="specialityid"  id="specialityid">
             <form:option value="0" label="Choose Speciality"/>
   			 <form:options items="${specialities}" />
		</form:select>
		</div>
		</div>
		
		 <div class="control-group">
          <!-- Medical registration number -->
          <div class="controls">
            <form:input length="10" id="mregno" name="mregno" path="regno" placeholder="Medical Registration Number" class="input-xlarge"/>
          </div>
        </div>
        		 <div class="control-group">
          <!-- referral code -->
          <div class="controls">
            <form:input length="10" id="refcode" name="refcode" path="referalcode" placeholder="Referral code" class="input-xlarge"/>
          </div>
        </div>
        </c:when>
        </c:choose>
  			   <div class="control-group">
          <!-- Gender -->
          <div class="controls">
      		<form:radiobutton id="male" class="input-xlarge" path="gender" value="M"/> &nbsp;Male &nbsp;
			<form:radiobutton id="female" class="input-xlarge" path="gender" value="F"/> &nbsp;Female  &nbsp;
		</div>
		</div>
		
		
		
	  <div class="control-group">
          <!-- City -->
          <div class="controls">
        <form:select class="input-xlarge" name="city" path="cityid"  id="city">
             <form:option value="0" label="Choose city"/>
   			 <form:options items="${cities}" />
		</form:select>
		</div>
		</div>
		

        <div class="control-group">
          <!-- Button -->
          <div class="controls">
            <button class="btn btn-success btn-large btn-block">Save</button>
          </div>
        </div>
      </fieldset>
    </form:form>
  </section>
  <script>
  $(document).ready(function() {
	  $(".js-example-basic-single").select2({dropdownCssClass : 'bigdrop'});
	});
  <c:if test="${not empty error}">

	$(document).ready(function(){
		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_DANGER,
               title: 'Profile',
               message: '${error}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    
	});

</c:if>
<c:if test="${not empty success}">
	$(document).ready(function(){
		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_SUCCESS,
               title: 'Profile',
               message: '${success}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    
	});

</c:if>
  </script>
  <script type='text/javascript'>
$('.nav >li.active').removeClass("active");
$('.nav #profile').addClass("active");
</script>
<%@ include file="footer.jsp"%>