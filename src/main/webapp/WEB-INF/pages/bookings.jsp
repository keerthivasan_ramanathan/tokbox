<%@ include file="header.jsp" %>
  <section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
          <c:choose>
                    <c:when test="${user.userType == 'U'}">
                    <c:set var="type" value="appointments"></c:set>
		 			<h1>Appointments</h1>
					 </c:when>
		 			<c:when test="${user.userType == 'S'}">
		 			  <c:set var="type" value="bookings"></c:set>
		 			<h1>Bookings</h1>
		 			</c:when>
		 </c:choose>
        </div>
      </div>
    </div>
  </section>
  <br>
    <!-- /header -->
    <!--  Dashboard div start -->
    <div class="container" style="min-height:700px;">
   <!--   <script>
        $(document).ready(function(){

            $("#calendar").fullCalendar({
            	header: {
    				left: 'prev,next today'
    			
    			},              
                defaultView: 'agendaWeek',
				slotDuration : '00:15:00',
				events: 'api/fetchbookings',
                eventClick: function(calEvent, jsEvent, view) {
                     alert(JSON.stringify(calEvent)); 
                }
						

            })
			


        });
   </script> -->

 <%--   <div id="calendar" style="width:970px;height:900px">
   </div> --%>
   		<div class="row">
   		   <c:if test="${fn:length(bookings) == 0}">
		   		  <h4 class="list-group-item-text">You have no ${type} as of now</h4>
			</c:if>
		   <div class="list-group">
		   <c:forEach items="${bookings}" var="booking">
		 	 <div class="list-group-item">
		 	  <c:choose>
                    <c:when test="${user.userType == 'U'}">
		   		 <h4 class="list-group-item-heading">${booking.slot.user.name}</h4>
		   		  <p class="list-group-item-text">Specialized in ${booking.slot.user.speciality.name}</p>
		   		  <p class="list-group-item-text">Booking Title :  ${booking.title}</p>
		   		   <a class="pull-right" href="joinsession?id=${booking.id}" class="btn btn-success btn-large btn-block" role="button">Start session</a>
		   		  <p class="list-group-item-text">Booking Desc :  ${booking.desc}</p>
		   		  <p class="list-group-item-text">Session starts at  ${booking.slot.startdate}</p>
		   		 
		   		 </c:when>
		   		  <c:when test="${user.userType == 'S'}">
		   		 <h4 class="list-group-item-heading">${booking.subscriber.name}</h4>
		   		  <p class="list-group-item-text">Booking Title :  ${booking.title}</p>
		   		   <a class="pull-right" href="joinsession?id=${booking.id}" class="btn btn-success btn-large btn-block" role="button">Start session</a>
		   		  <p class="list-group-item-text">Booking Desc :  ${booking.desc}</p>
		   		  <p class="list-group-item-text">Session starts at  ${booking.slot.startdate}</p>
		   		 </c:when>
		   		 </c:choose>	
		  	</div>
		  	</c:forEach>
			</div>
			
			</div>
			<div id="pagination"></div>
	</div>
<script type='text/javascript'>
var options = {
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			fetchbooking(page)
		},
		size : 'large',
		alignment : 'center'

	};

	$('#pagination').bootstrapPaginator(options);

	function fetchbooking(page) {
		var start = (page * 10) - 10;
		window.location.href="bookings?start="+start+"&range=10";
	}
$('.nav >li.active').removeClass("active");
$('.nav #bookings').addClass("active");

</script>
<script type="text/javascript">
 <c:if test="${not empty error}">


		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_DANGER,
               title: '${errortitle}',
               message: '${error}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    


</c:if>
<c:if test="${not empty msg}">

		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_SUCCESS,
               title: '${msgtitle}',
               message: '${msg}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    
	

</c:if>
</script>
<%@ include file="footer.jsp" %>