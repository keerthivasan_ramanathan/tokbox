
<html>

<head>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta charset="utf-8" /><meta name=Generator content="Microsoft Word 15 (filtered)">
<title>Your Medics</title>
<link rel="shortcut icon" type="image/x-icon"
	href="images/favicon.ico" /><style>
<style id="dynCom" type="text/css"><!-- --></style>
<script language="JavaScript"><!--
function msoCommentShow(anchor_id, com_id)
{
	if(msoBrowserCheck()) 
		{
		c = document.all(com_id);
		a = document.all(anchor_id);
		if (null != c && null == c.length && null != a && null == a.length)
			{
			var cw = c.offsetWidth;
			var ch = c.offsetHeight;
			var aw = a.offsetWidth;
			var ah = a.offsetHeight;
			var x  = a.offsetLeft;
			var y  = a.offsetTop;
			var el = a;
			while (el.tagName != "BODY") 
				{
				el = el.offsetParent;
				x = x + el.offsetLeft;
				y = y + el.offsetTop;
				}
			var bw = document.body.clientWidth;
			var bh = document.body.clientHeight;
			var bsl = document.body.scrollLeft;
			var bst = document.body.scrollTop;
			if (x + cw + ah / 2 > bw + bsl && x + aw - ah / 2 - cw >= bsl ) 
				{ c.style.left = x + aw - ah / 2 - cw; }
			else 
				{ c.style.left = x + ah / 2; }
			if (y + ch + ah / 2 > bh + bst && y + ah / 2 - ch >= bst ) 
				{ c.style.top = y + ah / 2 - ch; }
			else 
				{ c.style.top = y + ah / 2; }
			c.style.visibility = "visible";
}	}	}
function msoCommentHide(com_id) 
{
	if(msoBrowserCheck())
		{
		c = document.all(com_id);
		if (null != c && null == c.length)
		{
		c.style.visibility = "hidden";
		c.style.left = -1000;
		c.style.top = -1000;
		} } 
}
function msoBrowserCheck()
{
	ms = navigator.appVersion.indexOf("MSIE");
	vers = navigator.appVersion.substring(ms + 5, ms + 6);
	ie4 = (ms > 0) && (parseInt(vers) >= 4);
	return ie4;
}
if (msoBrowserCheck())
{
	document.styleSheets.dynCom.addRule(".msocomanchor","background: infobackground");
	document.styleSheets.dynCom.addRule(".msocomoff","display: none");
	document.styleSheets.dynCom.addRule(".msocomtxt","visibility: hidden");
	document.styleSheets.dynCom.addRule(".msocomtxt","position: absolute");
	document.styleSheets.dynCom.addRule(".msocomtxt","top: -1000");
	document.styleSheets.dynCom.addRule(".msocomtxt","left: -1000");
	document.styleSheets.dynCom.addRule(".msocomtxt","width: 33%");
	document.styleSheets.dynCom.addRule(".msocomtxt","background: infobackground");
	document.styleSheets.dynCom.addRule(".msocomtxt","color: infotext");
	document.styleSheets.dynCom.addRule(".msocomtxt","border-top: 1pt solid threedlightshadow");
	document.styleSheets.dynCom.addRule(".msocomtxt","border-right: 2pt solid threedshadow");
	document.styleSheets.dynCom.addRule(".msocomtxt","border-bottom: 2pt solid threedshadow");
	document.styleSheets.dynCom.addRule(".msocomtxt","border-left: 1pt solid threedlightshadow");
	document.styleSheets.dynCom.addRule(".msocomtxt","padding: 3pt 3pt 3pt 3pt");
	document.styleSheets.dynCom.addRule(".msocomtxt","z-index: 100");
}
// --></script>
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Helvetica;
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:"Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:Georgia;
	panose-1:2 4 5 2 5 4 5 2 3 3;}
@font-face
	{font-family:"Georgia Bold";
	panose-1:2 4 8 2 5 4 5 2 2 3;}
@font-face
	{font-family:Tahoma;
	panose-1:2 11 6 4 3 5 4 4 2 4;}
@font-face
	{font-family:"\@Arial Unicode MS";
	panose-1:2 11 6 4 2 2 2 2 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	line-height:115%;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoCommentText, li.MsoCommentText, div.MsoCommentText
	{mso-style-link:"Comment Text Char";
	margin-top:0in;
	margin-right:0in;
	margin-bottom:10.0pt;
	margin-left:0in;
	font-size:10.0pt;
	font-family:"Calibri","sans-serif";}
p.MsoAcetate, li.MsoAcetate, div.MsoAcetate
	{mso-style-link:"Balloon Text Char";
	margin:0in;
	margin-bottom:.0001pt;
	font-size:8.0pt;
	font-family:"Tahoma","sans-serif";}
p.Default, li.Default, div.Default
	{mso-style-name:Default;
	margin:0in;
	margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Helvetica","sans-serif";
	color:black;
	border:none;}
span.BalloonTextChar
	{mso-style-name:"Balloon Text Char";
	mso-style-link:"Balloon Text";
	font-family:"Tahoma","sans-serif";}
span.CommentTextChar
	{mso-style-name:"Comment Text Char";
	mso-style-link:"Comment Text";}
.MsoChpDefault
	{font-family:"Calibri","sans-serif";}
.MsoPapDefault
	{margin-bottom:10.0pt;
	line-height:115%;}
 /* Page Definitions */
 @page WordSection1
	{size:8.5in 11.0in;
	margin:1.0in 1.0in 1.0in 1.0in;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}
-->
</style>

</head>

<body lang=EN-US>

<div class=WordSection1>

<p class=MsoNormal align=center style='margin-top:0in;margin-right:28.3pt;
margin-bottom:0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:center;
text-indent:-14.2pt;line-height:150%;border:none'><u><span style='font-size:
12.0pt;line-height:150%;font-family:"Georgia Bold","serif";color:#00000A'>TERMS
&amp; CONDITIONS</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>1.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>GENERAL</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>This document is an electronic record in terms of Information
Technology Act, 2000 and rules made there under as applicable and the amended
provisions pertaining to electronic records in various statutes as amended by
the Information Technology Act, 2000. This electronic record is generated by a
computer system and does not require any physical or digital signatures.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-21.25pt;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>This document is published in accordance with the provisions of
Rule 3 (1) of the Information Technology (Intermediaries guidelines) Rules,
2011 that require publishing the rules and regulations, privacy policy and
Terms of Use for access or usage of </span><a href="http://www.yourmedics.in."><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:blue'>www.yourmedics.in.</span></a></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-21.25pt;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The domain names </span><a href="http://www.yourmedics.in"><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:blue'>www.yourmedics.in</span></a><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'> (&quot;</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Website</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>&quot;) and the Mobile Application
named </span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Your Medics</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'> (</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>"</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>Mobile App</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>"</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>) is owned and operated by Arvene
Healthcare Pvt. Ltd. (</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>"</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Company</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>"</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>)</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'> </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>a private limited company registered under the Companies Act,
2013, and having its registered office at G-38, Quest Offices Pvt Ltd.,
Technopolis, 5th Floor, Golf course road, Sector-54, Gurgaon, Haryana-122002,
INDIA, where such expression shall, unless repugnant to the context thereof, be
deemed to include its respective representatives, administrators, employees,
directors, officers, agents and their successors and assigns.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-21.25pt;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>d)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>For the purpose of these Terms of Use (</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>"</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>Terms</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>"</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>), wherever the context so
requires, </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>i)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The term</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Times New Roman","serif";color:#00000A'> </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>You</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Times New Roman","serif";color:#00000A'>,</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Times New Roman","serif";
color:#00000A'> </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Your</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>, </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>User</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Times New Roman","serif";
color:#00000A'> </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>&amp; </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>Subscriber</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'> shall mean any legal person or
entity accessing or using the Services provided on this Website/ Mobile App,
who is competent to enter into binding contracts, as per the provisions of the
Indian Contract Act, 1872, whether for himself or for anyone who has authorized
such person to submit his information in order to avail the services of the
Website/Mobile App ;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>ii)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The terms </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>We</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>,</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Times New Roman","serif";color:#00000A'> </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>Us</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>, </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>Our</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Times New Roman","serif";color:#00000A'> </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&amp; </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Your Medics</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'> shall mean the Website/ Mobile App and/or the Company, as the
context so requires</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>iii)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The term </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>'</span><b><span style='font-size:
12.0pt;line-height:150%;font-family:"Georgia","serif";color:#00000A'>Services</span></b><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Times New Roman","serif";color:#00000A'> </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>shall mean those Services offered on the Website/Mobile
Application by the Company, as enumerated under Clause 5.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>iv)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The terms</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'> </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>Party</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Times New Roman","serif";color:#00000A'> </span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>&amp; </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Parties</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Times New Roman","serif";
color:#00000A'> </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>shall respectively be used to
refer to the User and the Company individually and collectively, as the context
so requires</span><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>e)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The headings of each section in these Terms are only for the
purpose of organizing the various provisions under these Terms in an orderly
manner, and shall not be used by either Party to interpret the provisions
contained herein in any manner. Further, it is specifically agreed to by the
Parties that the headings shall have no legal or contractual value.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-21.25pt;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>f)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The use of the Website/ Mobile App by the User is solely
governed by these Terms as well as the Privacy Policy (</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>"</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>Policy</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>"</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Times New Roman","serif";color:#00000A'>)</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>, available at </span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia Bold","serif";color:#00000A'><a href="privacyPolicy"><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>Privacy Policy</span></a>&nbsp;</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>and any
modifications or amendments made thereto by the Company from time to time, at
its sole discretion. Visiting the home page of the Website/ Mobile App and/or
using any of the Services provided on the Website/ Mobile App shall be deemed
to signify the User</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>'</span><span style='font-size:
12.0pt;line-height:150%;font-family:"Georgia","serif";color:#00000A'>s
unequivocal acceptance of these Terms and the aforementioned Policy, and the
User expressly agrees to be bound by the same. The User expressly agrees and
acknowledges that the Terms and Policy are co-terminus, and that expiry/termination
of either one will lead to the termination of the other, save as provided in
Clause 3 hereunder.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-21.25pt;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>g)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The User unequivocally agrees that these Terms and the
aforementioned Policy constitute a legally binding agreement between the User
and the Company, and that the User shall be subject to the rules, guidelines,
policies, terms, and conditions applicable to any service that is provided by
the Website/ Mobile App, and that the same shall be deemed to be incorporated
into these Terms, and shall be treated as part and parcel of the same. The User
acknowledges and agrees that no signature or express act is required to make
these Terms and the Policy binding on the User, and that the User</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>s act of registering with  the
Website/ Mobile App constitutes the User</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s full and final acceptance of these Terms and the
aforementioned Policy.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-21.25pt;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>h)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The Company reserves the sole and exclusive right to amend or
modify these Terms without any prior permission or intimation to the User, and
the User expressly agrees that any such amendments or modifications shall come
into effect immediately. The User has a duty to periodically check the terms
and stay updated on its requirements.  If the User continues to use the
Website/ Mobile App following such a change, the User will be deemed to have
consented to any and all amendments / modifications made to the Terms. In so
far as the User complies with these Terms, he is granted a personal,
non-exclusive, non-transferable, revocable, limited privilege to enter and use
the Website/ Mobile App.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>2.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>ELIGIBILITY</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>     The User represents and
warrants that he is competent and eligible to enter into legally binding
agreements and that he has the requisite authority to bind himself to these
Terms, as determined solely by the provisions of the Indian Contract Act, 1872.
The User may not use this Website/ Mobile App if he is not competent to
contract under the Indian Contract Act, 1872, or is disqualified from doing so
by any other applicable law, rule or regulation currently in force.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>3.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>TERMS</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>These Terms shall continue to form a valid and
binding contract between the Parties, and shall continue to be in full force
and effect until: </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The User continues to access and use the Website/ Mobile App; or</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The Transaction between the Parties, if any, concludes to the
satisfaction of both Parties;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>Whichever is longer. The Parties agree that
certain portions of these Terms (</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>"</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Clauses</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>"</span><span style='font-size:
12.0pt;line-height:150%;font-family:"Georgia","serif";color:#00000A'>), such as
Clauses 11, 12, 13, 16 &amp; 18, shall continue to remain in full force and
effect indefinitely, even after the expiry or termination of these Terms as
contemplated herein.  </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>4.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>TERMINATION</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>The Company reserves the right, in its sole
discretion, to unilaterally terminate the User</span><span style='font-size:
12.0pt;line-height:150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s access to the Services offered on the Website and the Mobile
App, or any portion thereof, at any time, without notice or cause. The User
shall continue to be bound by these Terms, and it is expressly agreed to by the
Parties that the User shall not have the right to terminate these Terms till
the expiry of the same, as described in </span><span style='font-size:12.0pt;
line-height:150%;font-family:"Times New Roman","serif";color:#00000A'>3</span><span
lang=DE style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'> hereinabove. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>5.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>ONLINE/ MOBILE APPLICATION PLATFORM</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'><span style='text-decoration:none'>&nbsp;</span></span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The Website/ Mobile App is a platform where you can upload and
maintain your health records electronically.  The Website/Mobile App works on
an alert generating mechanism for taking the medicines, based on the inputs of
prescription provided by the User.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'><br>
 </span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Your Medics is a web/App based platform which is based on
patient centric features and provide following Services:</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&#10146;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Uploading medical history including:</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Basic information with the specific columns like
Name/Age/Sex/Body Weight/Height etc.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Generic information pertaining to the genetic disease in his/her
family.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Allergies (if any) </span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'> Past cured disease </span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Current Disease</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Current Medications</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&#10146;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Editing and updating medical history.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&#10146;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Setting up alarms for timing of medicine intake, using
Website/Mobile App.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&#10146;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Setting up SMS Services for timing of medicine intake, through a
third party gateway. [Please note that Your Medics will not have any control of
timely delivery of SMS etc.]</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>&#10146;<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Subscriber can access and retrieve his medical history and
medication.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;border:none'><u><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia Bold","serif";color:#00000A'><span
 style='text-decoration:none'>&nbsp;</span></span></u></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia Bold","serif";color:#00000A'>6.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>REGISTRATION</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:7.1pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>To fully avail the Services of the Application and use it,
registration by the User or an individual who is authorized by such User , is
required. </span><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>&nbsp;</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>As a part of
registration process you agree to provide Your Medics current, complete, and
accurate registration information as prompted to do and to maintain and update
this information as required to keep it updated, complete and accurate.</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>The Users can have multiple sub
accounts within one login account. You are required to register by providing
your entire medical history with the specific columns as mentioned below:</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Name, Email id, Phone number, Age, Sex, Body Weight, Height etc.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'> Family history of diseases.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Allergies: Any allergy that the User wants to save in terms of
drugs or food allergy.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Past cured diseases. User can upload past records, edit and save
the same. There is a provision to include information in date wise format.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Current Disease(s).</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Current medications: this includes all the current medications
that the User wants to upload for records purpose or alarm purpose.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>A summary of all the recorded data will be available which may
be useful in cases of discussion with doctors.</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>•<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>All the information provided shall be saved on a secure cloud
based platform.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:21.3pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Membership of this website is available only to those above the
age of 18 years barring those </span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>"</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Incompetent to Contract</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>"</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Times New Roman","serif";
color:#00000A'> </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>which <i>inter alia </i>include
insolvents. If  You are a minor and wish to use the Application, You may do so
through Your legal guardian and  Arvene Healthcare Pvt. Ltd. reserves the right
to terminate Your account on knowledge of You being a minor and having
registered on the Application or availing any of its Services. </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:21.3pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Further, at any time during Your use of this Application,
including but not limited to the time of registration, You are solely
responsible for protecting the confidentiality of Your username and password,
and any activity under the account shall be deemed to have been done by You. In
the case that You provide Us with false and/or inaccurate details or We have
reason to believe You have done so, We hold the right to permanently suspend
Your account.  </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:29.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-29.0pt;line-height:150%;page-break-after:avoid;border:none'><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A;text-transform:uppercase'>7.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A;text-transform:uppercase'>COMMUNICATION</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>By using this Website/ Mobile App, and
providing his contact information to the Company through the Website/ Mobile
App, the User hereby agrees and consents to receiving calls, autodialed and/or
pre-recorded message calls, e-mails and SMSs from the Company and/or any of its
affiliates or partners at any time, subject to the Policy. In the event that
the User wishes to stop receiving any such marketing or promotional calls /
email messages / text messages, the User may send an e-mail to the effect to </span><a
href="mailto:contactus@yourmedics.in"><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>contactus@yourmedics.in</span></a><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'> with the subject</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia Bold","serif";color:#00000A'> [Stop
Promos].</span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'> The User agrees and acknowledges that it may take up to seven
(7) business days for the Company to give effect to such a request by the
User.   </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>The User expressly agrees that notwithstanding
anything-contained hereinabove, he may be contacted by the Company or any of
its affiliates / partners relating to any service availed of by the User on the
Website/ Mobile App or anything pursuant thereto. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>It is expressly agreed to by the Parties that
any information shared by the User with the Company shall be governed by the
Policy.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:29.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-29.0pt;line-height:150%;page-break-after:avoid;border:none'><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A;text-transform:uppercase'>8.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A;text-transform:uppercase'>CHARGES</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>The use of this Website/ Mobile App by the
User, including browsing the Website/ Mobile App is free of cost. The Company
reserves the right to amend this no-fee policy and charge the User for the use
of the Website/Application. In such an event, the User will be intimated of the
same when he attempts to access the Website/Mobile App, and the User shall have
the option of declining to avail of the Services offered on the Website/Mobile
App. Any such change, if made, shall come into effect immediately upon such
change being notified to the User, unless specified otherwise. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:29.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-29.0pt;line-height:150%;page-break-after:avoid;border:none'><b><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A;text-transform:uppercase'>9.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span style='font-size:14.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A;text-transform:uppercase'>USER
OBLIGATIONS</span></u></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>The User agrees and acknowledges that he is a
restricted user of this Website/ Mobile App, and that he: </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>is bound not to cut, copy, distribute, modify, recreate, reverse
engineer, distribute, disseminate, post, publish or create derivative works
from, transfer, or sell any information or software obtained from the Website/
Mobile App. Any such use / limited use of the Website/ Mobile App will only be
allowed with the prior express written permission of the Company. For the
removal of doubt, it is clarified that unlimited or wholesale reproduction,
copying of the content for commercial or non-commercial purposes and
unwarranted modification of data and information contained on the Website/
Mobile App is expressly prohibited.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>agrees not to access (or attempt to access) the Website/ Mobile
App and/or the materials or Services by any means other than through the interface
provided by the Website/ Mobile App. The use of deep-link, robot, spider or
other automatic device, program, algorithm or methodology, or any similar or
equivalent manual process, to access, acquire, copy or monitor any portion of
the Website/ Mobile App or its content, or in any way reproduce or circumvent
the navigational structure or presentation of the Website/ Mobile App,
materials or any content, or to obtain or attempt to obtain any materials,
documents or information through any means not specifically made available
through the Website/ Mobile App will lead to suspension or termination of the
User</span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>s access to the Website/ Mobile
App, as detailed in Clause 10 herein below. The User acknowledges and agrees
that by accessing or using the Website/ Mobile App or any of the Services
provided therein, he may be exposed to content that he may consider offensive,
indecent or otherwise objectionable. The Company disclaims any and all
liabilities arising in relation to such offensive content on the Website/
Mobile App. The User expressly agrees and acknowledges that all the Services
displayed on the Website/ Mobile App are not owned by the Company/Website/
Mobile App, and that the same may be the exclusive property of certain third
parties who have chosen to market their Services through the Company</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>s Website/ Mobile App, and that
the Company is in no way responsible for the content of the same. The User may
however report any such offensive or objectionable content, which the Company
may then remove from the Website/ Mobile App, at its sole discretion.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>In places where Website/ Mobile App permits the User to post or
upload data/information, the User undertakes to ensure that such material is
not offensive or objectionable, and is in accordance with applicable laws. The
User expressly agrees that any such material that is deemed to be
objectionable/offensive may be removed from the Website/ Mobile App immediately
and without notice, and further that the User</span><span style='font-size:
12.0pt;line-height:150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s access to the Website/ Mobile App may also be permanently
revoked, at the sole discretion of the Company.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>d)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Further undertakes not to: </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Abuse, harass, threaten, defame, disillusion, erode, abrogate,
demean or otherwise violate the legal rights of any other person or entity;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Engage in any activity that interferes with or disrupts access
to the Website/ Mobile App or the Services provided therein (or the servers and
networks which are connected to the Website/ Mobile App);</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Impersonate any person or entity, or falsely state or otherwise
misrepresent his/her affiliation with a person or entity;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Publish, post, disseminate, any information which is grossly
harmful, harassing, blasphemous, defamatory, obscene,</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>pornographic, pedophilic,
libelous, invasive of another's privacy, hateful, or racially, ethnically
objectionable, disparaging, relating or encouraging money laundering or
gambling, or otherwise unlawful in any manner whatever under any law, rule or
regulation currently in force; or unlawfully threatening or unlawfully
harassing including but not limited to &quot;indecent representation of
women&quot; within the meaning of the Indecent Representation of Women
(Prohibition) Act, 1986;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Post any image/file/data that infringes the copyright, patent or
trademark of another person or legal entity;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Upload or distribute files that contain viruses, corrupted
files, or any other similar software or programs that may damage the operation
of the Website/ Mobile App;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Download any file posted/uploaded by another user of the
Website/ Mobile App that the User is aware, or should reasonably be aware,
cannot be legally distributed in such a manner;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>viii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Neither probe, scan or test the vulnerability of the Website/
Mobile App or any network connected to the Website/ Mobile App, nor breach the
security or authentication measures on the Website/ Mobile App or any network
connected to the Website/ Mobile App. The User may not reverse look-up, trace
or seek to trace any information relating to any other user of, or visitor to,
the Website/ Mobile App, or any other customer of the Website/ Mobile App,
including any user account maintained on the Website/ Mobile App not
operated/managed by the User, or exploit the Website/ Mobile App or information
made available or offered by or through the Website/ Mobile App, in any manner;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>ix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Disrupt or interfere with the security of, or otherwise cause
harm to, the Website/ Mobile App, systems resources, accounts, passwords,
servers or networks connected to or accessible through the Websites/ Mobile
Apps or any affiliated or linked Websites/ Mobile Apps;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>x.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Collect or store data about other users of the Website/ Mobile App.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Use the Website/ Mobile App or any material or content therein
for any purpose that is unlawful or prohibited by these Terms, or to solicit
the performance of any illegal activity or other activity which infringes the
rights of this Website/ Mobile App or any other third party(ies);</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Violate any code of conduct or guideline which may be applicable
for or to any particular or service offered on the Website/ Mobile App;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xiii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Violate any applicable laws, rules or regulations currently in
force within or outside India;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xiv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Violate any portion of these Terms or the Policy, including but
not limited to any applicable additional terms of the Website/ Mobile App
contained herein or elsewhere, whether made by amendment, modification, or
otherwise;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Threaten the unity, integrity, defence, security or sovereignty
of India, friendly relations with foreign states, or public order, or cause
incitement to the commission of any cognizable offence, or prevent the
investigation of any offence, or insult any other nation.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xvi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Publish, post, or disseminate information that is false,
inaccurate or misleading; </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xvii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Directly or indirectly offer, attempt to offer, trade, or
attempt to</span><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>&nbsp;</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>trade, any item
the dealing of which is prohibited or restricted in any manner under the
provisions of any applicable law, rule, regulation or guideline for the time
being in force.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xviii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Commit any act that causes the Company to lose (in whole or in
part) the Services of its internet service provider (&quot;</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>ISP</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>&quot;) or in any manner disrupts
the Services of any other supplier/service provider of the Company/Website/
Mobile App;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>xix.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Engage in advertising to, or solicitation of, other users of the
Website/ Mobile App to buy or sell any products or Services not currently
displayed on the Website/ Mobile App. The User may not transmit any chain
letters or unsolicited commercial or junk email/messages to other users via the
Website/ Mobile App or through any other internet based platform infringing the
reputation of the company or its Services. It shall be a violation of these
Terms to use any information obtained from the Website/ Mobile App in order to
harass, abuse, or harm another person, or in order to contact, advertise to,
solicit, or sell to another user of the Website/ Mobile App without the express
prior written consent of the Company. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>e)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The User expressly understands and agrees the following.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>i.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>In order to use the Services offered through the Website/ Mobile
App, You need to be a Registered User. You agree to provide us with accurate
and complete registration information. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>ii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>It is the sole responsibility of the User to inform Company of
any changes to that information. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>iii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Each registration is for a single individual only, unless
specifically designated otherwise on the registration page. Each registration
can hold multiple accounts as per provision of the Website/Application. The
number  of accounts will be limited to a maximum of 6 sub accounts for one
registration. All the terms are applicable to the sub accounts also.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>iv.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>You are responsible for maintaining the confidentiality of your
account credentials.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>v.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>You shall be responsible for all uses of your account, whether
or not authorized by You. You agree to immediately notify us of any
unauthorized access or use of Your account or password. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>vi.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>When a user registers on the Website/ Mobile App, You will be
asked to provide us with certain information including, without limitation,
Your name, username, contact number, date of birth, gender, and a valid email
address.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>vii.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>In addition to these Terms of Use, You understand and agree that
We may collect and disclose certain information about You to third parties. In
order to understand how We collect and use your information, please visit our
<a href="privacyPolicy"><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>Privacy Policy</span></a>.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:56.7pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>f)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The User hereby expressly authorizes the Company/Website/ Mobile
App to disclose any and all information relating to the User in the possession
of the Company/Website/ Mobile App to law enforcement or other government
officials, as the Company may in its sole discretion, believe necessary or
appropriate in connection with the investigation and/or resolution of possible
crimes, especially those involve personal injury and theft / infringement of
intellectual property. The User further understands that the Company/Website/
Mobile App might be directed to disclose any information (including the
identity of persons providing information or materials on the Website/ Mobile
App) as necessary to satisfy any judicial order, law, regulation or valid
governmental request. </span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The  User expressly agrees and acknowledges that the
Company/Website/ Mobile App has no obligation to monitor the materials posted
on the Website/ Mobile App, but that it has the right to remove or edit any
content that in its sole discretion violates, or is alleged to violate, any
applicable law or either the spirit or letter of these Terms. Notwithstanding
this right, the User remains solely responsible for the content of the
materials posted on the Website/ Mobile App by him/her. In no event shall the
Company/Website/ Mobile App assume or be deemed to have any responsibility or
liability for any content posted, or for any claims, damages or losses
resulting from use of any such content and/or the appearance of any content on
the Website/ Mobile App. The User hereby represents and warrants that he/she
has all necessary rights in and to all content provided as well as all
information contained therein, and that such content does not infringe any
proprietary or other rights of any third party(ies), nor does it contain any
libelous, tortuous, or otherwise unlawful or offensive material, and the User
hereby accepts full responsibility for any consequences that may arise due to
the publishing of any such material on the Website/ Mobile App. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:64.35pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>g)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The User agrees that:</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#333333'>i.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#333333;background:white'>Any content on this website and particularly
any such content relating to medical conditions and their treatment is solely
for general informational purposes/alert purposes and is not intended as, shall
not be construed to be, and is no substitute for the advise provided by a
qualified and practicing expert medical professional.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:78.0pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>ii.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'> He/She must never ignore qualified medical guidance or
treatment or postpone seeking qualified medical diagnosis or treatment because
of data on the Website/Mobile Application. The material on the Website/Mobile
Application should not be utilized in lieu of a appointment, call,
consultation, or guidance to, with, or from a qualified healthcare
professional, inclusive of a personal physician.</span></p>

<p class=MsoNormal style='margin-bottom:0in;margin-bottom:.0001pt;text-align:
justify;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>iii.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The User agrees that the Company may terminate the User</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>s access to or use of the Your
Medics system and Services at any time if we the Company is unable at any time
to determine or verify the User</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s qualifications or credentials.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:78.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-14.2pt;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#262626'>iv.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#262626'>The User will </span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>implement</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#262626'> and maintain appropriate administrative, physical and technical
safeguards to protect information within the Your Medics system from access,
use or alteration and will always use the user ID assigned to him or a member
of the User</span><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#262626'>'</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#262626'>s workforce. The
User is required to maintain appropriate security with regard to all personnel,
systems, and administrative processes used by him or members of his workforce
to transmit, store and process electronic health information through the use of
the Your Medics system. The User will immediately notify the Company of any
breach or suspected breach of the security of the Your Medics system, or any
unauthorized use or disclosure of information within or obtained from the Your
Medics system, and will take such action to mitigate the breach or suspected
breach as the Company may direct, and will cooperate with the Company in
investigating and mitigating such breach.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:78.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-14.2pt;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#262626'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#262626'>v.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#262626'>The User represents and warrants that he/she will, at all times
during the use of the Your Medics system and thereafter, comply with all laws
directly or indirectly applicable that may now or hereafter govern the
gathering, use, transmission, processing, receipt, reporting, disclosure,
maintenance, and storage of the patient information, and use best efforts to
cause all persons or entities under his/her direction or control to comply with
such laws, including but not limited to the Information Technology Act, 2000
and the rules made thereunder. The User is at all times during the use of the
Your Medics system and thereafter, solely responsible for obtaining and
maintaining all patient consents, and all other legally necessary consents or
permissions required to disclose, process, retrieve, transmit, and view the
patient information. The Company does not assume any responsibility for the
User</span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#262626'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#262626'>s use or misuse of patient
information or other information transmitted, monitored, stored or received
while using Your Medics. The Company reserves the right to amend or delete any
material (along with the right to revoke any membership or restrict access)
that in its sole discretion violates the above.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:78.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:-14.2pt;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#262626'>&nbsp;</span></p>

<p class=MsoNormal style='margin:0in;margin-bottom:.0001pt;text-align:justify;
text-indent:0in;line-height:150%;border:none'><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#262626'>vi.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#262626'>The Company makes no representations concerning the
completeness, accuracy or utility of any information or Services offers in the
Your medics system. The Company has no liability for the consequences to the
Doctor or his/her patients.</span></p>

<p class=MsoNormalCxSpMiddle style='margin-top:0in;margin-right:0in;margin-bottom:
0in;margin-left:.5in;margin-bottom:.0001pt;line-height:normal;border:none'><span
style='font-size:12.0pt;font-family:"Georgia","serif";color:#262626'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:78.0pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#262626'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:0in;margin-bottom:0in;
margin-left:78.0pt;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:29.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-29.0pt;line-height:150%;page-break-after:avoid;border:none'><b><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A;text-transform:uppercase'>10.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span style='font-size:14.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A;text-transform:uppercase'>SUSPENSION
OF USER ACCESS AND ACTIVITY </span></u></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>Notwithstanding other legal remedies that may
be available to it, the Company may in its sole discretion limit the User</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>s access and/ or activity by
immediately removing the User</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s access credentials either temporarily or indefinitely, or
suspend/ terminate the User</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s membership, and/or refuse to provide User with access to the
Website/ Mobile App, without being required to provide the User with notice or
cause:</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>If the User is in breach of any of these Terms or the Policy;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>If the User has provided wrong, inaccurate, incomplete or
incorrect information;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>If the User</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s actions may cause any harm, damage or loss to the other users
or to the Website/ Mobile App/Company,.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:29.0pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-29.0pt;line-height:150%;page-break-after:avoid;border:none'><b><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A;text-transform:uppercase'>11.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><b><u><span style='font-size:14.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A;text-transform:uppercase'>INDEMNITY
AND LIMITATIONS</span></u></b></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>The User hereby expressly agrees to defend,
indemnify and hold harmless the Website/ Mobile App and the Company, its
parent, subsidiaries, affiliates, employees, directors, officers, agents and
their successors and assigns and against any and all claims, liabilities,
damages, losses, costs and expenses, including attorney's fees, caused by or
arising out of claims based upon the User's actions or inactions, including but
not limited to any warranties, representations or undertakings, or in relation
to the non-fulfillment of any of the User</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s obligations under this Agreement, or arising out of the User's
infringement of any applicable laws, rules and regulations, including but not
limited to infringement of intellectual property rights, payment of statutory
dues and taxes, claims of libel, defamation, violation of rights of privacy or
publicity, loss of service by other subscribers, or the infringement of any
other rights of a third party. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>In no event shall the Company/Website/ Mobile
App be liable to compensate the User or any third party for any special,
incidental, indirect, consequential or punitive damages whatsoever, including
those resulting from loss of use, data or profits, whether or not foreseeable,
and whether or not the Company/Website/ Mobile App had been advised of the
possibility of such damages, or based on any theory of liability, including
breach of contract or warranty, negligence or other tortuous action, or any
other claim arising out of or in connection with the User</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>s use of or access to the Website/
Mobile App and/or the products, Services or materials contained therein.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>The limitations and exclusions in this section
apply to the maximum extent permitted by applicable law, and the Parties
expressly agree that in the event of any statute, rule, regulation or amendment
coming into force that would result in the Company/Website/ Mobile App
incurring any form of liability whatsoever, these Terms and the Policy will
stand terminated one (1) day before the coming into effect of such statute,
rule, regulation or amendment. It is further agreed to by the Parties that the
contents of this Section shall survive even after the termination or expiry of
the Terms and/or Policy.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><u><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'><span
 style='text-decoration:none'>&nbsp;</span></span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>12.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>INTELLECTUAL PROPERTY RIGHTS </span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>Unless expressly agreed to in writing, nothing
contained herein shall give the User a right to use any of the Website/ Mobile
App</span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>s trade names, trademarks, service
marks, logos, domain names, information, questions, answers, solutions, reports
and other distinctive brand features, save according to the provisions of these
Terms. All logos, trademarks, brand names, service marks, domain names,
including material, designs, and graphics created by and developed by the
Website/ Mobile App and other distinctive brand features of the Website/ Mobile
App are the property of the Company. Furthermore, with respect to the Website/
Mobile App created by the Company, the Company shall be the exclusive owner of
all the designs, graphics and the like, related to the Website/ Mobile App. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>The User may not use any of the intellectual
property displayed on the Website/ Mobile App in any manner that is likely to
cause confusion among existing or prospective users of the Website/ Mobile App,
or that in any manner disparages or discredits the Company/Website/ Mobile App,
to be determined in the sole discretion of the Company.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>The User is further aware that any
reproduction or infringement of the intellectual property of owners of such
rights by the User may result in legal action being initiated against the User
by the respective owners of the intellectual property so reproduced / infringed
upon. It is agreed to by the Parties that the contents of this Section shall
survive even after the termination or expiry of the Terms and/or Policy.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>13.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>DISCLAIMER OF WARRANTIES AND LIABILITIES</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>Except as otherwise expressly stated on the Website/ Mobile App,
all Services offered on the Website/ Mobile App are offered on an &quot;as
is&quot; basis without any warranty whatsoever, either express or implied.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The Content on the Website/ Mobile App should not be substituted
for medical or health advice. Nothing on the Website/ Mobile App is aimed at
medical diagnosis or treatment or as a suggestion of a course of treatment for
a specific Subscriber or User. The Content does not aim to be an auxiliary for
qualified medical guidance, analysis, or treatment.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>d)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The Company/ Website/ Application Or any of its agent, servant
or assigns shall not be liable for any direct or indirect, willful or
otherwise, act or omission that can be attributed to the Services on the
Website/ Application. Or any of its agent, servant or assigns have any
liability whatsoever in case any third party claims, demands, suit, actions, or
other proceedings are initiated against any of its personnel or any other
person engaged by the Company. The Company/Website/ Mobile App cannot be held responsible
in a court of law, for any situations of damages including but not limiting to
medical damage or negligence arising in a User</span><span style='font-size:
12.0pt;line-height:150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s practice whether the situation arose as a result of
information or knowledge obtained from Your Medics or not or related to the
Services provided or agreed to be provided by Your Medics. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>e)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The User agrees and undertakes that he is accessing t</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>h</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>e Website/ Mobile App and he is
using his</span><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'> </span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>best and</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'> </span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>prudent judgment before availing
any service listed on the Website/ Mobile </span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>A</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>pp, or accessing/using any information displayed thereon.  </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>f)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>In case the User uploads any details of a third person related
or known to the User, uploading such details would deemed to have been done
after the User obtaining express consent from such third person.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>g)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The Website/ Mobile App and the Company accepts no liability for
any errors or omissions, whether on behalf of itself or third parties, or for
any damage caused to the User, the User</span><span style='font-size:12.0pt;
line-height:150%;font-family:"Georgia","serif";color:#00000A'>'</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>s belongings, or any third party, resulting from the use or
misuse of service availed of by the User from the Website/ Mobile App.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>h)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The Company/Website/ Mobile App does not guarantee that the
functions and Services contained in the Website/ Mobile App will be
uninterrupted or error-free, or that the Website/ Mobile App or its server will
be free of viruses or other harmful components, and the User hereby expressly
accepts any and all associated risks involved with the User</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>'</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>s use of the Website/ Mobile App.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>i)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>It is further agreed to by the Parties that the contents of this
Section shall survive even after the termination or expiry of the Terms and/or
Policy.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>14.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>SUBMISSIONS</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>Any comments, ideas, suggestions, initiation,
or any other content contributed by the User to the Company or this Website/
Mobile App will be deemed to include a royalty-free, perpetual, irrevocable,
nonexclusive right and license for the Company to adopt, publish, reproduce,
disseminate, transmit, distribute, copy, use, create derivative works, display
worldwide, or act on such content, without additional approval or
consideration, in any media, or technology now known or later developed, for
the full term of any rights that may exist in such content, and the User hereby
waives any claim to the contrary. The User hereby represents and warrants that
he owns or otherwise controls all of the rights to the content contributed to
the Website/ Mobile App, and that use of such content by the Company/Website/
Mobile App does not infringe upon or violate the rights of any third party. In
the event of any action initiated against the Company/Website/ Mobile App by
any such affected third party, the User hereby expressly agrees to indemnify
and hold harmless the Company/Website/ Mobile App, for its use of any such
information provided to it by the User. The Company reserves its right to
defend itself in any such legal disputes that may arise, and recover the costs
incurred in such proceedings from the User.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>15.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>FORCE MAJEURE</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>Neither the Company nor the Website/ Mobile
App shall be liable for damages for any delay or failure to perform its
obligations hereunder if such delay or failure is due to cause beyond its
control or without its fault or negligence, due to Force Majeure events
including but not limited to acts of war, acts of God, earthquake, riot,
sabotage, labor shortage or dispute, internet interruption, technical failure,
breakage of sea cable, hacking, piracy, cheating, illegal or unauthorized.</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;line-height:150%;
border:none'><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><b><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia Bold","serif";color:#00000A'>16.<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span></b><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>DISPUTE RESOLUTION AND JURISDICTION</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>It is expressly agreed to by the Parties
hereto that the formation, interpretation and performance of these Terms and
any disputes arising here from will be resolved through a two-step Alternate
Dispute Resolution (</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>"</span><span style='font-size:
12.0pt;line-height:150%;font-family:"Georgia Bold","serif";color:#00000A'>ADR</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>"</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>) mechanism. It is further agreed
to by the Parties that the contents of this Section shall survive even after
the termination or expiry of the Terms and/or Policy. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:28.35pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Mediation</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>: In case of any dispute between
the parties, the Parties will attempt to resolve the same amicably amongst
themselves, to the mutual satisfaction of both Parties. In the event that the
Parties are unable to reach such an amicable solution within thirty (30) days
of one Party communicating the existence of a dispute to the other Party, the
dispute will be resolved by arbitration, as detailed herein below; </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:35.45pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-21.3pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Arbitration</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>. In the event that the
Parties are unable to amicably resolve a dispute by mediation, said dispute
will be referred to arbitration by a sole arbitrator to be appointed by the
Company, and the award passed by such sole arbitrator will be valid and binding
on both Parties. The Parties shall bear their own costs for the proceedings,
although the sole arbitrator may, in his/her sole discretion, direct either
Party to bear the entire cost of the proceedings. The arbitration shall be
conducted in English, and the seat of Arbitration shall be the city of Gurgaon
in the state of Haryana, India. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>c)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>The Parties expressly agree that the Terms, Policy and any other
agreements entered into between the Parties are governed by the laws, rules and
regulations of India, and that the Courts at Gurgaon shall have exclusive
jurisdiction over any disputes arising between the Parties. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-.05pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>17.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>NOTICES</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;line-height:
150%;border:none'><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia","serif";color:#00000A'>Any and all communication relating to any
dispute or grievance experienced by the User may be communicated to the Company
by the User reducing the same to writing, and sending the same to the
registered office of the Company by Registered Post Acknowledgement Due / Speed
Post Acknowledgement Due (</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia Bold","serif";color:#00000A'>RPAD / SPAD</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif";
color:#00000A'>)</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:14.2pt;margin-bottom:.0001pt;text-align:justify;text-indent:
-14.2pt;line-height:150%;border:none'><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>&nbsp;</span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia Bold","serif";color:#00000A'>18.<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><u><span style='font-size:12.0pt;line-height:150%;font-family:
"Georgia Bold","serif";color:#00000A'>MISCELLANEOUS PROVISIONS</span></u></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>a)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Entire Agreement</span><span style='font-size:12.0pt;line-height:
150%;font-family:"Georgia","serif";color:#00000A'>: These Terms, read with the
Policy form the complete and final contract between the User and the Company
with respect to the subject matter hereof and supersedes all other
communications, representations and agreements (whether oral, written or
otherwise) relating thereto; </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>b)<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif";
color:#00000A'>Waiver</span><span style='font-size:12.0pt;line-height:150%;
font-family:"Georgia","serif";color:#00000A'>: The failure of either Party at
any time to require performance of any provision of these Terms shall in no
manner affect such Party's right at a later time to enforce the same. No waiver
by either Party of any breach of these Terms, whether by conduct or otherwise,
in any one or more instances, shall be deemed to be or construed as a further
or continuing waiver of any such breach, or a waiver of any other breach of
these Terms. </span></p>

<p class=MsoNormal style='margin-top:0in;margin-right:28.3pt;margin-bottom:
0in;margin-left:0in;margin-bottom:.0001pt;text-align:justify;text-indent:0in;
line-height:150%;border:none'><span style='font-family:"Georgia","serif"'>c)<span
style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span style='font-size:12.0pt;line-height:150%;font-family:"Georgia Bold","serif"'>Severability</span><span
style='font-size:12.0pt;line-height:150%;font-family:"Georgia","serif"'>: If
any provision/clause of these Terms is held to be invalid, illegal or
unenforceable by any court or authority of competent jurisdiction, the
validity, legality and enforceability of the remaining provisions/clauses of
these Terms shall in no way be affected or impaired thereby, and each such
provision/clause of these Terms shall be valid and enforceable to the fullest
extent permitted by law. In such case, these Terms shall be reformed to the
minimum extent necessary to correct any invalidity, illegality or
unenforceability, while preserving to the maximum extent the original rights,
intentions and commercial expectations of the Parties hereto, as expressed
herein.</span></p>

</div>

</body>

</html>
