<%@ include file="header.jsp"%>

<section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
		 <h1>Availability Schedule</h1>
        </div>
      </div>
    </div>
  </section>
<!-- /header -->
<!--  Dashboard div start -->
<br>
<div class="container" style="min-height:700px;">

		<div class="row">
			<div class="col-md-6 col-lg-6">
			<form:form id="schedule" action="saveavailability"  modelAttribute="schedule" method="POST">
				<form:input  class="datepick" placeholder="Start Date"  type="text" id="startdatepicker" path="startdateStr"/>&nbsp;&nbsp;
				<form:input  class="datepick" placeholder="End Date"  type="text" id="enddatepicker" path="enddateStr"/>
				<table class="table table-striped ">
					<thead>
						<tr>
							<th>S.NO</th>
							<th>Day Name</th>
							<th>Sessions</th>
							<th>Start time (24 hr HH:mm format)</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Sunday</td>
							<td>
							<form:input type="text" class="sessions" name="suntimings" path="suntimings" id="sun_sessions" data-role="tagsinput" />
							</td>
							<td><input type="text" id="sun_starttime" /></td>
							<td><a href="javascript:validateTime('sun_starttime')">Add</a></td>
						</tr>
						<tr>
							<td>2</td>
							<td>Monday</td>
							<td>
									<form:input class="sessions" name="montimings" path="montimings" id="mon_sessions" data-role="tagsinput" />
							</td>
							<td><input type="text" id="mon_starttime" /></td>
							<td><a href="javascript:validateTime('mon_starttime')">Add</a></td>
						</tr>
						<tr>
							<td>3</td>
							<td>Tuesday</td>
							<td>
									<form:input class="sessions" name="tuetimings" path="tuetimings" id="tue_sessions" data-role="tagsinput" />
							</td>
							<td><input type="text" id="tue_starttime" /></td>
							<td><a href="javascript:validateTime('tue_starttime')">Add</a></td>
						</tr>
						<tr>
							<td>4</td>
							<td>Wednesday</td>
							<td>
									<form:input class="sessions" name="wedtimings" path="wedtimings" id="wed_sessions" data-role="tagsinput" />
							</td>
							<td><input type="text" id="wed_starttime" /></td>
							<td><a href="javascript:validateTime('wed_starttime')">Add</a></td>
						</tr>
						<tr>
							<td>5</td>
							<td>Thursday</td>
							<td>
									<form:input class="sessions" name="thutimings" path="thutimings" id="thu_sessions" data-role="tagsinput" />
							</td>
							<td><input type="text" id="thu_starttime" /></td>
							<td><a href="javascript:validateTime('thu_starttime')">Add</a></td>
						</tr>
						<tr>
							<td>6</td>
							<td>Friday</td>
							<td>
									<form:input class="form-control" name="fritimings" path="fritimings" id="fri_sessions" data-role="tagsinput" />
							</td>
							<td><input type="text" id="fri_starttime" /></td>
							<td><a href="javascript:validateTime('fri_starttime')">Add</a></td>
						</tr>
						<tr>
							<td>7</td>
							<td>Saturday</td>
							<td>
								<form:input class="form-control sessions" name="sattimings" path="sattimings" id="sat_sessions" data-role="tagsinput" />
							</td>
							<td><input type="text" id="sat_starttime" /></td>	
							<td><a href="javascript:validateTime('sat_starttime')">Add</a></td>
						</tr>
					</tbody>
				</table>
				<input id="savebtn" type="submit" name="action" value="Save"/>
				</form:form>
			</div>
		</div>
	</div>

  <script type='text/javascript'>
$('.nav >li.active').removeClass("active");
$('.nav #availability').addClass("active");

</script>



<%@ include file="footer.jsp"%>