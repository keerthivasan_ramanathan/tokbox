<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Tokbox</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
 <link rel="stylesheet" href="css/bootstrap.min.css"> 
     <link rel="stylesheet" href="css/bootstrap.css"> 
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/sl-slide.css">
     <link rel="stylesheet" href="css/mystyle.css">
     <link rel="stylesheet" href="css/bootstrap-dialog.css">
      <link rel="stylesheet" href="css/select2.css">
      <link rel="stylesheet" href="css/select2-bootstrap.css">
      <link rel='stylesheet' href='css/fullcalendar.css' />
       <link rel='stylesheet' href='css/jquery-ui.css' />
      <link rel='stylesheet' href='css/bootstrap-tagsinput.css' />
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
</head>

<body>

	

    <!--Header-->
    <header class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a id="logo" class="pull-left" href="main"></a>
                <div class="nav-collapse collapse pull-center">
                    <ul class="nav">
                    <c:choose>
                    <c:when test="${user.userType == 'U'}">
                        <li id="main" class="active"><a href="main">Home</a></li>
                        <li id="bookings"><a href="bookings">Appointments</a></li>
                        <li id="specialists"><a href="specialists">Specialists</a></li> 
                     </c:when>
                       <c:when test="${user.userType == 'S'}">
                        <li id="main" class="active"><a href="main">Home</a></li>
                        <li id="availability"><a  href="availability">Availability</a></li>
                        <li id="bookings"><a  href="bookings">Bookings</a></li> 
                        <li  id="schedule"><a href="viewcalendar?uid=${user.userid}">View Schedule</a></li> 
                     </c:when>
                    </c:choose>
                        <li id="profile"><a href="profile">Profile</a></li>
                         <li ><a class="pull-right" href="<c:url value="j_spring_security_logout" />">Log out</a></li>
                    </ul>      
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </header>
<script src="js/jquery-1.9.1.min.js"></script>
<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- Required javascript files for Slider -->
<script src="js/jquery.ba-cond.min.js"></script>
<script src="js/jquery.slitslider.js"></script>
<script src="js/bootstrap-dialog.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/moment.js"></script>
<script src="js/fullcalendar.js"></script>
<script src="js/bootstrap-paginator.js"></script>
<script src="js/bootstrap-tagsinput.js"></script>
<script src="js/jquery-ui.js"></script>
<script>
function validateTime(id){
	var time = $("#"+id).val();
	var timepattern = new RegExp("^(0[0-9]|1[0-9]|2[0-4]):[0-5][0-9]$");
	var res = timepattern.test(time);
	if(res){
		var splits = id.split("_");
		$('#'+splits[0]+'_sessions').tagsinput('add',time);
	}
	else{
		alert('Please enter valid time');
	}
	$("#"+id).val('');
}

$(function() {
    $(".datepick").datepicker({
      showOn: "button",
      buttonImage: "images/calendar.gif",
      buttonImageOnly: true,
      buttonText: "Select date",
      dateFormat: 'yy-mm-dd'
    });
  });
  
</script>
