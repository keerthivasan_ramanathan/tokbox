<%@ include file="outerheader.jsp" %>
<section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
          <h1>Registration</h1>
        </div>
      </div>
    </div>
  </section>

  <section id="registration-page" class="container">
    <form:form class="center has-validation-callback" action='adduser' method="POST" modelAttribute="user">
      <fieldset class="registration-form">
        <div class="control-group">
          <!-- Name -->
          <div class="controls">
            <form:input type="text" id="name" name="name" path="name" placeholder="Name" class="input-xlarge"
            data-validation="length alpha" 
		 data-validation-length="3-20" 
		 data-validation-error-msg="Name has to be alphabets only (3-20 chars)"/>
          </div>
        </div>

        <div class="control-group">
          <!-- E-mail -->
          <div class="controls">
            <form:input type="text" id="email" name="email" path="email" placeholder="E-mail" class="input-xlarge" data-validation="email"/>
          </div>
        </div>

       
        <div class="control-group">
          <!-- Password-->
          <div class="controls">
            <input type="password" id="password" name="password_confirmation" path="password" placeholder="Password" class="input-xlarge" data-validation="length" data-validation-length="min8"
            data-validation-error-msg="Passwords should be atleast 8 characters"/>
          </div>
        </div>

        <div class="control-group">
          <!-- Password -->
          <div class="controls">
            <input type="password" id="password_confirm" name="password" placeholder="Password (Confirm)" class="input-xlarge" data-validation="confirmation"
              data-validation-error-msg="Passwords didn't match">
          </div>
        </div>
        
         <div class="control-group">
          <!-- Mobile -->
          <div class="controls">
            <form:input length="10" id="mobile" name="mobile" path="mobile" placeholder="Mobile number" class="input-xlarge" data-validation="length number" data-validation-length="10"
             data-validation-error-msg="Mobile Number should be 10 digit number"/>
         
          </div>
        </div>
        <c:set var="currentPage" value="${requestScope['javax.servlet.forward.request_uri']}"></c:set>
        <c:set var="splitURI" value="${fn:split(currentPage, '/')}"/> 
        <c:set var="lastValue" value="${splitURI[fn:length(splitURI)-1]}"/>
     
        <c:choose>
        
        <c:when test="${lastValue == 'spec_register.html'}">
        <div class="control-group">
          <!-- Speciality -->
          <div class="controls">
        <form:select class="input-xlarge" name="speciality" path="specialityid"  id="specialityid">
             <form:option value="0" label="Choose Speciality"/>
   			 <form:options items="${specialities}" />
		</form:select>
		</div>
		</div>
		
		 <div class="control-group">
          <!-- Medical registration number -->
          <div class="controls">
            <form:input length="10" id="mregno" name="mregno" path="regno" placeholder="Medical Registration Number" class="input-xlarge" data-validation="length number" data-validation-length="5"
            data-validation-error-msg="Medical Registration Number should be 5 digit number"/>
          </div>
        </div>
        		 <div class="control-group">
          <!-- referral code -->
          <div class="controls">
            <form:input length="10" id="refcode" name="refcode" path="referalcode" placeholder="Referral code" class="input-xlarge"/>
          </div>
        </div>
        </c:when>
        </c:choose>
  			   <div class="control-group">
          <!-- Gender -->
          <div class="controls">
      		<form:radiobutton id="male" class="input-xlarge" path="gender" value="M" checked="true"/> &nbsp;Male &nbsp;
			<form:radiobutton id="female" class="input-xlarge" path="gender" value="F" /> &nbsp;Female  &nbsp;
		</div>
		</div>
		
		
		
	  <div class="control-group">
          <!-- City -->
          <div class="controls">
        <form:select class="input-xlarge" name="city" path="cityid"  id="city">
             <form:option value="0" label="Choose city"/>
   			 <form:options items="${cities}" />
		</form:select>
		</div>
		</div>
		

        <div class="control-group">
          <!-- Button -->
          <div class="controls">
            <button class="btn btn-primary btn-large btn-block">Register</button>
          </div>
        </div>
      </fieldset>
    </form:form>
  </section>
 
  <%@ include file="outerfooter.jsp" %>