<%@ include file="header.jsp"%>


<section class="title">
	<div class="container">
		<div class="row-fluid">
			<div class="span6">
				<h3>${owner.name}- Calendar</h3>
				<h5>${owner.speciality.name}</h5>
			</div>
		</div>
	</div>
</section>
<br>
<!-- /header -->
<!--  Dashboard div start -->
<div class="container" style="min-height: 700px;">


	<div id="calendar" style="width: 970px; height: 900px"></div>
</div>

<div id="dialog-form" title="Add Booking Information">
	<p class="validateTips">All form fields are required.</p>

	<form id="bookingform" method="POST" action="addbookinginfo">
		<fieldset>
			<label for="name">Title</label>
			 <input type="text" name="title" id="title" class="text ui-widget-content ui-corner-all">
			  <label for="email">Description</label> <input type="text" name="desc"
				id="desc" class="text ui-widget-content ui-corner-all"> 
			<input	type="hidden" name="slotid" id="slotid"> 
			<input type="hidden" name="subscriberid" id="subscriberid">
			<!-- Allow form submission with keyboard without duplicating the dialog button -->
			<input type="submit" tabindex="-1" 
				style="position: absolute; top: -1000px">
		</fieldset>
	</form>
</div>

<script>

$(document).ready(function() {
	title = $( "#title" ),
	desc = $( "#desc" );
	tips = $( ".validateTips" );
	
	function checkLength( o, n, min, max ) {
	    if ( o.val().length > max || o.val().length < min ) {
	      o.addClass( "ui-state-error" );
	      updateTips( "Length of " + n + " must be between " +
	        min + " and " + max + "." );
	      return false;
	    } else {
	      return true;
	    }
	  }
	
	function updateTips( t ) {
	      tips
	        .text( t )
	        .addClass( "ui-state-highlight" );
	      setTimeout(function() {
	        tips.removeClass( "ui-state-highlight", 1500 );
	      }, 500 );
	    }
	
	bookNow = function(){
		  var valid = true;
	      valid = valid && checkLength( title, "title", 3, 20 );
	      valid = valid && checkLength( desc, "notes", 6, 30 );
	      if(valid){
	    	  $("#bookingform").submit();
	      }
	     
	};

	dialog = $("#dialog-form").dialog({
			autoOpen : false,
			height : 400,
			width : 350,
			modal : false,
			buttons : {
				"Book Now" : bookNow,
				Cancel : function() {
					dialog.dialog("close");
				}
			},
			close : function() {
				form[0].reset();
			}
		});
	
	form = dialog.find( "form" );

});
	

		$(document).ready(function() {
	
			$("#calendar").fullCalendar({
				header : {
				//left: 'prev,next today'
				},
				defaultView : 'agendaWeek',
				slotDuration : '00:15:00',
				events : function(start, end, timezone, callback) {
					$.ajax({
						url : 'api/fetchavailability?uid=${param.uid}',
						dataType : 'json',
						data : {
							start : start.format('YYYY-MM-DD'),
							end : end.format('YYYY-MM-DD')
						},
						success : function(eventsdata) {
							var events = [];
							for (var i = 0; i < eventsdata.length; i++) {
								events.push(eventsdata[i]);
							}
							callback(events);
						}
					});
	
				},
				eventClick : function(calEvent, jsEvent, view) {
					if(calEvent.title != 'Booked'){
					$("#slotid").val(calEvent.id);
					$("#subscriberid").val('${user.userid}');
					dialog.dialog( "open" );
					}else{
						 BootstrapDialog.show({
				               type:  BootstrapDialog.TYPE_DANGER,
				               title: 'Booked already!',
				               message: 'This session is already booked.Please try any other slot',
				               buttons: [{
				                   label: 'OK',
				                   action: function(dialog) {
				                       dialog.close();
				                   }
				               }]
				           });   
					}
				}
			})
		});
</script>

<script type='text/javascript'>
	$('.nav >li.active').removeClass("active");
	$('.nav #schedule').addClass("active");
</script>
<%@ include file="footer.jsp"%>