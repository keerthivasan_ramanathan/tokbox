<%@ include file="header.jsp"%>

<div class="container" style="min-height:700px;">
	<div class="row">
		<form:form class="center" action='searchspecialists' method="POST"
			modelAttribute="speciality" id="searchform">
			<fieldset>
				<form:select class="input-xlarge" name="speciality" path="id"
					id="specialityid">
					<form:option value="0" label="Choose Speciality" />
					<form:options items="${specialities}" />
				</form:select>
				<button type="submit" class="btn btn-primary">Search</button>
			</fieldset>
			<input type="hidden" name="start" id="start" />
		</form:form>
	</div>

	<div class="widget widget-popular">
		<h4>Search results based on criteria</h4>
		<div class="row">
			<c:forEach items="${specialists}" var="specialist">

				<div class="col-md-8">
					<div class="panel-heading">
						<h3 class="panel-title">${specialist.name},${specialist.speciality.name}</h3>
					</div>
					<div class="panel-body">${specialist.city.name}</div>
				</div>
				<div class="col-md-4">
					<a class="btn btn-success btn-small"
						href="viewcalendar?uid=${specialist.userid}">View Calendar</a>
				</div>
			</c:forEach>
		</div>

		<div id="pagination"></div>
	</div>
</div>


<script type='text/javascript'>
	var options = {
		currentPage : '${currentpage}',
		totalPages : '${pageno}',
		onPageClicked : function(e, originalEvent, type, page) {
			searchFormSubmit(page)
		},
		size : 'large',
		alignment : 'center'

	};

	$('#pagination').bootstrapPaginator(options);

	function searchFormSubmit(page) {
		var start = (page * 10) - 10;
		$("#start").val(start);
		$("#searchform").submit();
	}
	
	$('.nav >li.active').removeClass("active");
	$('.nav #specialists').addClass("active");
</script>
<%@ include file="footer.jsp"%>