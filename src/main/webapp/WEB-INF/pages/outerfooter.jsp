
<!-- JS files  -->
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- Required javascript files for Slider -->
<script src="js/jquery.ba-cond.min.js"></script>
<script src="js/jquery.slitslider.js"></script>
<script src="js/bootstrap-dialog.js"></script>
<script src="js/select2.min.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/jquery.form-validator.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#errorBox").hide();
		$("#btnLogin").click(function() {
			$("#loginModal").slideToggle("slow");
		});
		
		

		if ($(".error").text().length > 0) {
			$(".pageContent").css("opacity", "0.4");
		} else {
			$(".pageContent").css("opacity", "1");
		}
		$(".pageContent").click(function() {
			if ($(".pageContent").css("opacity") == "0.4") {
				$(".pageContent").css("opacity", "1");
				$(".error").css("display", "none");
			}
		})
	});
	function myFunction() {
		window
				.open(
						"termsAndConditions",
						"Terms and Conditions",
						"width=800, height=500,top=100,left=300,location=no,menubar=no,scrollbars=yes,toolbar=no,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0");
	}
	function contactUs() {
		window
				.open(
						"contactUs",
						"Contact Us",
						"width=500, height=300,top=100,left=400,location=no,menubar=no,scrollbars=yes,toolbar=no,directories=0,titlebar=0,toolbar=0,location=0,status=0,menubar=0");
	}
	function Submit() {
		if (isRequired("email") && validEmail("email")
				&& isRequired("password") && minChar("password")) {
			return true;
		} else {
			return false;
		}
	}
</script>
 <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
     <script>
  $(document).ready(function() {
	  $(".js-example-basic-single").select2({dropdownCssClass : 'bigdrop'});
	});
  <c:if test="${not empty error}">

	$(document).ready(function(){
		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_DANGER,
               title: 'Registration failed',
               message: '{error}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    
	});

</c:if>
<c:if test="${not empty msg}">
	$(document).ready(function(){
		 BootstrapDialog.show({
               type:  BootstrapDialog.TYPE_SUCCESS,
               title: 'Registration successful',
               message: '${msg}',
               buttons: [{
                   label: 'OK',
                   action: function(dialog) {
                       dialog.close();
                   }
               }]
           });    
	});

</c:if>
  </script>
    <!-- SL Slider -->
<script type="text/javascript"> 
$(function() {
    var Page = (function() {
        var $navArrows = $( '#nav-arrows' ),
        slitslider = $( '#slider' ).slitslider({
            autoplay : true
        }),

        init = function() {
            initEvents();
        },
        initEvents = function() {
            $navArrows.children( ':last' ).on( 'click', function() {
                slitslider.next();
                return false;
            });

            $navArrows.children( ':first' ).on( 'click', function() {
                slitslider.previous();
                return false;
            });
        };

        return { init : init };

    })();

    Page.init();
});
</script>
<script>

  $.validate({
    modules : 'date, security'

  });

</script>
<!-- /SL Slider -->
</body>
</html>