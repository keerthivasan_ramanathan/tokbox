<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>, 
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Tokbox</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/sl-slide.css">
     <link rel="stylesheet" href="css/mystyle.css">
     <link rel="stylesheet" href="css/bootstrap-dialog.css">
      <link rel="stylesheet" href="css/select2.css">
      <link rel="stylesheet" href="css/select2-bootstrap.css">
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <style>
    
.modal-backdrop{position:fixed;top:0;right:0;bottom:0;left:0;z-index:1040;background-color:#000000;}.modal-backdrop.fade{opacity:0;}
.modal-backdrop,.modal-backdrop.fade.in{opacity:0.8;filter:alpha(opacity=80);}
.modal{position:fixed;top:10%;left:50%;z-index:1050;width:560px;margin-left:-280px;background-color:#ffffff;border:1px solid #999;border:1px solid rgba(0, 0, 0, 0.3);*border:1px solid #999;-webkit-border-radius:6px;-moz-border-radius:6px;border-radius:6px;-webkit-box-shadow:0 3px 7px rgba(0, 0, 0, 0.3);-moz-box-shadow:0 3px 7px rgba(0, 0, 0, 0.3);box-shadow:0 3px 7px rgba(0, 0, 0, 0.3);-webkit-background-clip:padding-box;-moz-background-clip:padding-box;background-clip:padding-box;outline:none;}.modal.fade{-webkit-transition:opacity .3s linear, top .3s ease-out;-moz-transition:opacity .3s linear, top .3s ease-out;-o-transition:opacity .3s linear, top .3s ease-out;transition:opacity .3s linear, top .3s ease-out;top:-25%;}
.modal.fade.in{top:10%;}
.modal-header{padding:9px 15px;border-bottom:1px solid #eee;}.modal-header .close{margin-top:2px;}
.modal-header h3{margin:0;line-height:30px;}
.modal-body{position:relative;overflow-y:auto;max-height:400px;padding:15px;}
.modal-form{margin-bottom:0;}
.modal-footer{padding:14px 15px 15px;margin-bottom:0;text-align:right;background-color:#f5f5f5;border-top:1px solid #ddd;-webkit-border-radius:0 0 6px 6px;-moz-border-radius:0 0 6px 6px;border-radius:0 0 6px 6px;-webkit-box-shadow:inset 0 1px 0 #ffffff;-moz-box-shadow:inset 0 1px 0 #ffffff;box-shadow:inset 0 1px 0 #ffffff;*zoom:1;}.modal-footer:before,.modal-footer:after{display:table;content:"";line-height:0;}
.modal-footer:after{clear:both;}
.modal-footer .btn+.btn{margin-left:5px;margin-bottom:0;}
.modal-footer .btn-group .btn+.btn{margin-left:-1px;}
.modal-footer .btn-block+.btn-block{margin-left:0;}
body {
	position: relative; float: left; margin: 0; height: 667px; width: 100%;
}

.err {
	padding: 1%;
	/* margin-bottom: 20px; */ border: 2px solid transparent;
	border-radius: 12px; color: #000; background-color: #E9EDF8;
	border-color: #B2B9D0; position: absolute; z-index: 2; width: 300px;
	text-align: center; height: 100px; font-family: segoe ui light;
	font-size: 18px; padding-top: 75px; top: calc(( 100% - 104px)/2);
	left: calc(( 100% - 304px)/2);
}

.msg {
	padding: 15px; margin-bottom: 20px; border: 1px solid transparent;
	border-radius: 4px; color: #31708f; background-color: #d9edf7;
	border-color: #bce8f1;
}

.pageContent {
	width: 100%; height: 100%; position: relative; float: left;
}

.headerContent {
	width: 100%; height: 500px; position: relative; float: left;
	background: url('images/BG.jpg');
}

.headerContent .headerSection1 {
	height: 100%; width: 50%; position: relative; float: left;
	text-align: center;
}

.headerContent .headerSection2 {
	height: 100%; width: 22%; position: relative; float: left;
	text-align: right; padding-top: 25px; padding-right: 3%;
}

.headerContent .headerSection2 a {
	font-family: 'segoe UI light'; font-size: 22px; text-decoration: none;
	color: #228FB0;
}

.headerContent .headerSection3 {
	height: 100%; width: 25%; position: relative; float: left;
	padding-top: 25px;
}

.headerContent .headerSection3 a {
	font-family: 'segoe UI light'; font-size: 22px; text-decoration: none;
	color: #228FB0;
}

.upperBodyContent {
	width: 100%; height: 25%; position: relative; float: left;
	text-align: center;
}

.upperBodyContentHeader {
	margin-top: 5%; font-size: 40px; font-family: segoe UI light;
	color: #fff;
}

.upperBodyContentText {
	width: 70%; margin: 0 15%; color: #fff; font-family: segoe UI light;
	font-size: 18px;
}

.bodyContent {
	width: 100%; height: auto; background-color: #fff; position: relative;
	float: left; margin-bottom: 20px;
}

.lowerBodyContent {
	width: 100%; height: auto; background-color: #E9EDF8;
	position: relative; float: left;
}

.footerContent {
	width: 100%; height: auto; background-color: #349DBA;
	position: relative; float: left;
}

.header {
	width: 100%; background-color: #fff; height: 18%; margin-top: 2%;
	position: relative;
}

.signUpButton {
	width: 25%; margin-top: 15px; height: 45px; background-color: #fff;
	margin-left: calc(75%/ 2); color: #228FB0; font-size: 25px;
	line-height: 1.8em;
}

.whyMedics {
	width: 100%; text-align: center; margin: 20px 0; font-size: 34px;
	font-family: segoe UI light;
}

.whyMedicsSectionLeft {
	float: left; width: 22%; padding: 0 2% 0 12%; text-align: center;
}

.whyMedicsSectionCentre {
	float: left; width: 22%; padding: 0 2% 0 2%; text-align: center;
}

.whyMedicsSectionRight {
	float: left; width: 22%; padding: 0 14% 0 2%; text-align: center;
}

.description {
	font-size: 17px; font-family: segoe ui light;
}

.lowerBodyHead {
	width: 100%; text-align: center; margin: 15px 0; font-size: 34px;
	font-family: segoe UI light;
}

.row {
	float: left; width: 100%; margin-bottom: 20px;
}

.left, .right {
	float: left; width: 50%;
}

.leftcomment {
	font-size: 17px; font-family: segoe ui light; margin-left: 45%;
	margin-right: 5%; border: 1px solid black; border-radius: 26px;
	height: 100px; padding: 10px;
}

.leftcomment p {
	margin: 0; text-align: right; clear: both;
}

.rightcomment {
	font-size: 17px; font-family: segoe ui light; margin-right: 45%;
	margin-left: 5%; border: 1px solid black; border-radius: 26px;
	height: 100px; padding: 10px;
}

.rightcomment p {
	margin: 0; text-align: right; clear: both;
}

.commentImage {
	width: 50px; float: right; margin: 0% -10% 0 0; border: 1px transparent;
	border-radius: 25px; clear: both; height: 50px;
}

.footerHeader {
	width: 100%; text-align: center; margin: 20px 0; font-size: 34px;
	font-family: segoe UI light; color: #fff;
}

.footerImages {
	width: 62%; padding-left: calc(( 100% - 680px)/2); padding-bottom: 30px;
	float: right;
}

.footerBorder {
	background-color: #0C7592; float: left; height: 3px; width: 100%;
}

.footerImage {
	float: left; margin-right: 3%; width: 21%; max-width: 145px;
}

.footerLinks {
	float: right; width: 66%; color : #fff; font-family : segoe ui light;
	font-size : 18px; margin-left: 15%; padding-top: 10px; color: #fff;
	font-family: segoe ui light; font-size: 18px;
}

.footerLink {
	float: left; width: 12%; text-align: center;
}

.loginModal {
	position: absolute; z-index: 1; width: 20%; background: #E9EDF8;
	height: 250px; text-align: center; right: 16%; top: 19%;
	font-family: segoe ui light; font-size: 24px; display: none;
}

.loginHead {
	height: 15%; padding-top: 4%;
}

.loginBody {
	height: 50%;
}

.loginBody input {
	width: 87%; height: 25%; font-size: .7em; margin-top: 5%;
	padding-left: 3%;
}

.loginFooter {
	height: 34%; background-color: #CEDBEC;
}

.loginFooter input {
	width: 84%; height: 50%; font-size: 20px; background-color: #349DBA;
	color: #fff; font-family: segoe ui light; margin-top: 6%;
}

@media ( min-width : 520px) and (max-width: 700px) {
	.signUpButton {
		width: 35%; margin-left: calc(62%/ 2);
	}
	.left {
		width: 100%; margin-bottom: 20px;
	}
	.leftcomment {
		margin-left: 5%; height: 70px;
	}
	.rightcomment {
		margin-right: 5%; height: 70px;
	}
	.right {
		width: 100%;
	}
	.commentImage {
		width: 8%; margin: -1% -6% 0 0;
	}
	.lowerBodyContent {
		height: auto;
	}
}

@media ( min-width : 360px) and (max-width: 519px) {
	.signUpButton {
		width: 50%; margin-left: calc(58%/ 2);
	}
	.left {
		width: 100%; margin-bottom: 20px;
	}
	.leftcomment {
		margin-left: 5%; height: 70px;
	}
	.rightcomment {
		margin-right: 5%; height: 70px;
	}
	.right {
		width: 100%;
	}
	.commentImage {
		width: 10%; margin: -1% -6% 0 0;
	}
	.lowerBodyContent {
		height: auto;
	}
}

@media only screen and (max-width: 359px) {
	.signUpButton {
		width: 70%; margin-left: calc(30%/ 2);
	}
	.headerContent .headerSection1 {
		width: 40%;
	}
	.headerContent .headerSection2 {
		width: 27%;
	}
}

@media ( min-width : 380px) and (max-width: 460px) {
	.footerImage {
		width: 100% !important; max-width: 145px; margin-bottom: 10px;
	}
	.footerImages {
		width: 86% !important; padding-left: calc(( 100% - 320px)/2) !important;
	}
	.footerimg, .footerLastImg {
		margin-top: 0 !important;
	}
}

@media ( min-width : 300px) and (max-width: 559px) {
	.whyMedicsSectionLeft {
		width: 96%; padding: 0 1.5% 0 2%;
	}
	.whyMedicsSectionCentre {
		width: 96%; padding: 0 1.5% 0 1.5%;
	}
	.whyMedicsSectionRight {
		width: 96%; padding: 0 2% 0 1.5%;
	}
	.footerImage {
		width: 55%; max-width: 145px;
	}
	.footerImages {
		width: 80%; padding-left: calc(( 100% - 320px)/2);
	}
	.footerimg {
		clear: both; margin-top: 10px;
	}
	.footerLastImg {
		margin-top: 10px;
	}
	.footerLinks {
		width: 74%; margin-left: 5%; font-size: 14px;
	}
	.footerLink {
		width: 14%;
	}
}

@media ( min-width : 950px) and (max-width: 1170px) {
	.leftcomment {
		margin-left: 30%; height: 100px;
	}
	.rightcomment {
		margin-right: 30%; height: 100px;
	}
}

@media ( min-width : 810px) and (max-width: 949px) {
	.leftcomment {
		margin-left: 20%;
	}
	.rightcomment {
		margin-right: 20%;
	}
	.footerImage {
		width: 25%; max-width: 145px;
	}
	.footerImages {
		width: 66%; padding-left: calc(( 100% - 640px)/2);
	}
}

@media ( min-width : 701px) and (max-width: 809px) {
	.left {
		width: 100%; margin-bottom: 20px;
	}
	.leftcomment {
		margin-left: 5%; height: 60px;
	}
	.rightcomment {
		margin-right: 5%; height: 60px;
	}
	.right {
		width: 100%;
	}
	.commentImage {
		width: 8%; margin: -1% -6% 0 0;
	}
	.lowerBodyContent {
		height: auto;
	}
	.footerImage {
		width: 35%; max-width: 145px;
	}
	.footerImages {
		width: 70%; padding-left: calc(( 100% - 320px)/2);
	}
	.footerimg {
		clear: both; margin-top: 10px;
	}
	.footerLastImg {
		margin-top: 10px
	}
}

@media ( min-width : 560px) and (max-width: 701px) {
	.footerImage {
		width: 45%; max-width: 145px;
	}
	.footerImages {
		width: 70%; padding-left: calc(( 100% - 320px)/2);
	}
	.footerimg {
		clear: both; margin-top: 10px;
	}
	.footerLastImg {
		margin-top: 10px
	}
}
    </style>
</head>

<body>

	

    <!--Header-->
    <header class="navbar navbar-fixed-top">
        <div class="navbar-inner">
            <div class="container">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <a id="logo" class="pull-left" href="index.html"></a>
                <div class="nav-collapse collapse pull-right">
                    <ul class="nav">
                        <li id="homebtn" class="active"><a href="home">Home</a></li>
                        <li id="registerbtn"><a href="register.html">Register</a></li> 
                        <li id="loginbtn" class="login">
                            <a data-toggle="modal" href="#loginForm"><i class="icon-lock"></i></a>
                        </li>
                  		 <li id="specbtn"><a href="spec_register.html">For Specialists</a></li>
                    
                    </ul>      
                </div><!--/.nav-collapse -->
            </div>
        </div>
    </header>
    <!--  Login form -->
<div class="modal hide fade in" id="loginForm" aria-hidden="false">
    <div class="modal-header">
        <i class="icon-remove" data-dismiss="modal" aria-hidden="true"></i>
        <h3>Login Form</h3>
    </div>
    <!--Modal Body-->
    <div class="modal-body">
        <form  action="<c:url value='/j_spring_security_check' />" method="post" id="form-login">
            <input name="username" type="text" class="input-xlarge" placeholder="Email" data-validation="email"><br>
            <input name="password" type="password" class="input-xlarge" placeholder="Password" data-validation="required">
            <label class="checkbox">
                <input type="checkbox" name="_spring_security_remember_me"> Remember me
            </label>
            <button type="submit" class="btn btn-primary">Sign in</button>
             
        </form>
    </div>
    </div>
