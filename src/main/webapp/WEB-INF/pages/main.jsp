<%@ include file="header.jsp" %>
    <!-- /header -->
    <!--  Dashboard div start -->
      <section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
		 <h1>Dashboard</h1>
        </div>
      </div>
    </div>
  </section>
  <br>
    <div class="container" style="min-height:700px;">
      <c:choose>
      <c:when test="${user.userType == 'U'}">
     <c:set var="heading" value="Appointments"></c:set>
     </c:when>
      <c:when test="${user.userType == 'S'}">
     <c:set var="heading" value="Bookings"></c:set>
     </c:when>
     </c:choose>
      <h4 class="list-group-item-heading">${heading}</h4>
      <p class="list-group-item-text">You have ${bookings_count} ${heading}</p>
	</div>
<%@ include file="footer.jsp" %>