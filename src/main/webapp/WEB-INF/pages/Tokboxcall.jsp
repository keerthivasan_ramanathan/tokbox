<%@ include file="header.jsp"%>
<script src="//static.opentok.com/webrtc/v2.2/js/TB.min.js"></script>
<script type="text/javascript">
	var apiKey = '${apikey}';
	var sessionId ='${sessionid}';
	var token = '${token}';
</script>
<script>
	window.onload = function() {
		// Initialize an OpenTok Session object
		var session = TB.initSession(sessionId);

		// Initialize a Publisher, and place it into the element with id="publisher"
		var publisher = TB.initPublisher(apiKey, 'publisher', {width: '100%', height: '100%', insertMode: 'replace'});

		// Attach event handlers
		session.on({

			// This function runs when session.connect() asynchronously completes
			sessionConnected : function(event) {
				// Publish the publisher we initialzed earlier (this will trigger 'streamCreated' on other
				// clients)
				session.publish(publisher);
			},

			// This function runs when another client publishes a stream (eg. session.publish())
			streamCreated : function(event) {
				// Create a container for a new Subscriber, assign it an id using the streamId, put it inside
				// the element with id="subscribers"
				var subContainer = document.createElement('div');
				subContainer.id = 'stream-' + event.stream.streamId;
				document.getElementById('subscribers')
						.appendChild(subContainer);
				// Subscribe to the stream that caused this event, put it inside the container we just made
				session.subscribe(event.stream, subContainer);
			}

		});

		// Connect to the Session using the 'apiKey' of the application and a 'token' for permission
		session.connect(apiKey, token);
	}
</script>
<section class="title">
    <div class="container">
      <div class="row-fluid">
        <div class="span6">
		 <h1>Video chat</h1>
        </div>
      </div>
    </div>
  </section>
  <br>
  <div class="container" style="min-height:700px;">
  <div id="publisher" style="min-height:500px;min-width:600px;"></div>
  </div>
<%@ include file="footer.jsp"%>