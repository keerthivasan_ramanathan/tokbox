/*
SQLyog Community v9.63 
MySQL - 5.5.27 : Database - tokbox
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tokbox` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tokbox`;

/*Table structure for table `availability` */

DROP TABLE IF EXISTS `availability`;

CREATE TABLE `availability` (
  `userid` int(11) DEFAULT NULL,
  `dayid` int(1) DEFAULT NULL,
  `starttime` varchar(5) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `availability` */

insert  into `availability`(`userid`,`dayid`,`starttime`,`created`) values (96,1,'12:00','2015-11-06 00:42:50'),(96,2,'13:00','2015-11-06 00:42:50'),(96,3,'14:00','2015-11-06 00:42:50'),(96,4,'15:00','2015-11-06 00:42:50'),(96,5,'16:00','2015-11-06 00:42:50'),(96,6,'00:30','2015-11-06 00:42:50'),(96,7,'02:00','2015-11-06 00:42:50');

/*Table structure for table `booking` */

DROP TABLE IF EXISTS `booking`;

CREATE TABLE `booking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `descr` varchar(40) DEFAULT NULL,
  `slotid` int(11) DEFAULT NULL,
  `subscriberid` int(11) DEFAULT NULL,
  `toksession` varchar(200) DEFAULT NULL,
  `pubtkn` varchar(500) DEFAULT NULL,
  `subtkn` varchar(500) DEFAULT NULL,
  `expiry` bigint(20) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `slotid` (`slotid`),
  KEY `subscriber` (`subscriberid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `booking` */

insert  into `booking`(`id`,`title`,`descr`,`slotid`,`subscriberid`,`toksession`,`pubtkn`,`subtkn`,`expiry`,`created`) values (1,'Test','Video call',49,95,'2_MX40NTM3MDY1Mn5-MTQ0Njc1MTA3NDYyM34vbjBwSUFEdkdDa1ZtWHRtUkRqNTAzL1d-UH4',NULL,'T1==cGFydG5lcl9pZD00NTM3MDY1MiZzaWc9ZDliNWEyMzBjYzlhYjhjNDE0Njg5ODc4OWIwMTAzYmI1NDUzNzExMDpzZXNzaW9uX2lkPTJfTVg0ME5UTTNNRFkxTW41LU1UUTBOamMxTVRBM05EWXlNMzR2YmpCd1NVRkVka2REYTFadFdIUnRVa1JxTlRBekwxZC1VSDQmY3JlYXRlX3RpbWU9MTQ0Njc1MTExMCZub25jZT0tMTQ2NjE0NDk5OSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNDQ2ODM3NTEw',1446751800,'2015-11-06 00:43:37');

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `cityid` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cityid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `cities` */

insert  into `cities`(`cityid`,`name`,`created`) values (1,'Chennai','2015-10-17 15:21:03'),(2,'Bangalore','2015-10-17 15:21:10'),(3,'Mumbai','2015-10-17 15:21:17'),(4,'Pune','2015-10-17 15:21:23'),(5,'Delhi','2015-10-17 15:21:28'),(6,'Hyderabad','2015-10-17 15:21:40');

/*Table structure for table `days` */

DROP TABLE IF EXISTS `days`;

CREATE TABLE `days` (
  `id` int(1) NOT NULL,
  `shortname` char(3) DEFAULT NULL,
  `varchar` varchar(10) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `days` */

insert  into `days`(`id`,`shortname`,`varchar`,`created`) values (1,'SUN','Sunday','2015-10-26 22:39:59'),(2,'MON','Monday','2015-10-26 22:40:07'),(3,'TUE','Tuesday','2015-10-26 22:40:18'),(4,'WED','Wednesday','2015-10-26 22:40:33'),(5,'THU','Thursday','2015-10-26 22:40:49'),(6,'FRI','Friday','2015-10-26 22:41:00'),(7,'SAT','Saturday','2015-10-26 22:41:12');

/*Table structure for table `slot` */

DROP TABLE IF EXISTS `slot`;

CREATE TABLE `slot` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ownerid` int(11) DEFAULT NULL,
  `startdate` datetime DEFAULT NULL,
  `enddate` datetime DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` char(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

/*Data for the table `slot` */

insert  into `slot`(`id`,`ownerid`,`startdate`,`enddate`,`created`,`status`) values (1,96,'2015-11-01 12:00:00','2015-11-01 12:30:00','2015-11-06 00:42:50','A'),(2,96,'2015-11-08 12:00:00','2015-11-08 12:30:00','2015-11-06 00:42:50','A'),(3,96,'2015-11-15 12:00:00','2015-11-15 12:30:00','2015-11-06 00:42:50','A'),(4,96,'2015-11-22 12:00:00','2015-11-22 12:30:00','2015-11-06 00:42:50','A'),(5,96,'2015-11-29 12:00:00','2015-11-29 12:30:00','2015-11-06 00:42:50','A'),(6,96,'2015-12-06 12:00:00','2015-12-06 12:30:00','2015-11-06 00:42:50','A'),(7,96,'2015-12-13 12:00:00','2015-12-13 12:30:00','2015-11-06 00:42:50','A'),(8,96,'2015-12-20 12:00:00','2015-12-20 12:30:00','2015-11-06 00:42:50','A'),(9,96,'2015-12-27 12:00:00','2015-12-27 12:30:00','2015-11-06 00:42:50','A'),(10,96,'2016-01-03 12:00:00','2016-01-03 12:30:00','2015-11-06 00:42:50','A'),(11,96,'2015-11-02 13:00:00','2015-11-02 13:30:00','2015-11-06 00:42:50','A'),(12,96,'2015-11-09 13:00:00','2015-11-09 13:30:00','2015-11-06 00:42:50','A'),(13,96,'2015-11-16 13:00:00','2015-11-16 13:30:00','2015-11-06 00:42:50','A'),(14,96,'2015-11-23 13:00:00','2015-11-23 13:30:00','2015-11-06 00:42:50','A'),(15,96,'2015-11-30 13:00:00','2015-11-30 13:30:00','2015-11-06 00:42:50','A'),(16,96,'2015-12-07 13:00:00','2015-12-07 13:30:00','2015-11-06 00:42:50','A'),(17,96,'2015-12-14 13:00:00','2015-12-14 13:30:00','2015-11-06 00:42:50','A'),(18,96,'2015-12-21 13:00:00','2015-12-21 13:30:00','2015-11-06 00:42:50','A'),(19,96,'2015-12-28 13:00:00','2015-12-28 13:30:00','2015-11-06 00:42:50','A'),(20,96,'2016-01-04 13:00:00','2016-01-04 13:30:00','2015-11-06 00:42:50','A'),(21,96,'2015-11-03 14:00:00','2015-11-03 14:30:00','2015-11-06 00:42:50','A'),(22,96,'2015-11-10 14:00:00','2015-11-10 14:30:00','2015-11-06 00:42:50','A'),(23,96,'2015-11-17 14:00:00','2015-11-17 14:30:00','2015-11-06 00:42:50','A'),(24,96,'2015-11-24 14:00:00','2015-11-24 14:30:00','2015-11-06 00:42:50','A'),(25,96,'2015-12-01 14:00:00','2015-12-01 14:30:00','2015-11-06 00:42:50','A'),(26,96,'2015-12-08 14:00:00','2015-12-08 14:30:00','2015-11-06 00:42:50','A'),(27,96,'2015-12-15 14:00:00','2015-12-15 14:30:00','2015-11-06 00:42:50','A'),(28,96,'2015-12-22 14:00:00','2015-12-22 14:30:00','2015-11-06 00:42:50','A'),(29,96,'2015-12-29 14:00:00','2015-12-29 14:30:00','2015-11-06 00:42:50','A'),(30,96,'2016-01-05 14:00:00','2016-01-05 14:30:00','2015-11-06 00:42:50','A'),(31,96,'2015-11-04 15:00:00','2015-11-04 15:30:00','2015-11-06 00:42:50','A'),(32,96,'2015-11-11 15:00:00','2015-11-11 15:30:00','2015-11-06 00:42:50','A'),(33,96,'2015-11-18 15:00:00','2015-11-18 15:30:00','2015-11-06 00:42:50','A'),(34,96,'2015-11-25 15:00:00','2015-11-25 15:30:00','2015-11-06 00:42:50','A'),(35,96,'2015-12-02 15:00:00','2015-12-02 15:30:00','2015-11-06 00:42:50','A'),(36,96,'2015-12-09 15:00:00','2015-12-09 15:30:00','2015-11-06 00:42:50','A'),(37,96,'2015-12-16 15:00:00','2015-12-16 15:30:00','2015-11-06 00:42:50','A'),(38,96,'2015-12-23 15:00:00','2015-12-23 15:30:00','2015-11-06 00:42:50','A'),(39,96,'2015-12-30 15:00:00','2015-12-30 15:30:00','2015-11-06 00:42:50','A'),(40,96,'2015-11-05 16:00:00','2015-11-05 16:30:00','2015-11-06 00:42:50','A'),(41,96,'2015-11-12 16:00:00','2015-11-12 16:30:00','2015-11-06 00:42:50','A'),(42,96,'2015-11-19 16:00:00','2015-11-19 16:30:00','2015-11-06 00:42:50','A'),(43,96,'2015-11-26 16:00:00','2015-11-26 16:30:00','2015-11-06 00:42:50','A'),(44,96,'2015-12-03 16:00:00','2015-12-03 16:30:00','2015-11-06 00:42:50','A'),(45,96,'2015-12-10 16:00:00','2015-12-10 16:30:00','2015-11-06 00:42:50','A'),(46,96,'2015-12-17 16:00:00','2015-12-17 16:30:00','2015-11-06 00:42:50','A'),(47,96,'2015-12-24 16:00:00','2015-12-24 16:30:00','2015-11-06 00:42:50','A'),(48,96,'2015-12-31 16:00:00','2015-12-31 16:30:00','2015-11-06 00:42:50','A'),(49,96,'2015-11-06 00:30:00','2015-11-06 01:00:00','2015-11-06 00:42:50','B'),(50,96,'2015-11-13 00:30:00','2015-11-13 01:00:00','2015-11-06 00:42:50','A'),(51,96,'2015-11-20 00:30:00','2015-11-20 01:00:00','2015-11-06 00:42:50','A'),(52,96,'2015-11-27 00:30:00','2015-11-27 01:00:00','2015-11-06 00:42:50','A'),(53,96,'2015-12-04 00:30:00','2015-12-04 01:00:00','2015-11-06 00:42:50','A'),(54,96,'2015-12-11 00:30:00','2015-12-11 01:00:00','2015-11-06 00:42:50','A'),(55,96,'2015-12-18 00:30:00','2015-12-18 01:00:00','2015-11-06 00:42:50','A'),(56,96,'2015-12-25 00:30:00','2015-12-25 01:00:00','2015-11-06 00:42:50','A'),(57,96,'2016-01-01 00:30:00','2016-01-01 01:00:00','2015-11-06 00:42:50','A'),(58,96,'2015-11-07 02:00:00','2015-11-07 02:30:00','2015-11-06 00:42:50','A'),(59,96,'2015-11-14 02:00:00','2015-11-14 02:30:00','2015-11-06 00:42:50','A'),(60,96,'2015-11-21 02:00:00','2015-11-21 02:30:00','2015-11-06 00:42:50','A'),(61,96,'2015-11-28 02:00:00','2015-11-28 02:30:00','2015-11-06 00:42:51','A'),(62,96,'2015-12-05 02:00:00','2015-12-05 02:30:00','2015-11-06 00:42:51','A'),(63,96,'2015-12-12 02:00:00','2015-12-12 02:30:00','2015-11-06 00:42:51','A'),(64,96,'2015-12-19 02:00:00','2015-12-19 02:30:00','2015-11-06 00:42:51','A'),(65,96,'2015-12-26 02:00:00','2015-12-26 02:30:00','2015-11-06 00:42:51','A'),(66,96,'2016-01-02 02:00:00','2016-01-02 02:30:00','2015-11-06 00:42:51','A');

/*Table structure for table `specialists` */

DROP TABLE IF EXISTS `specialists`;

CREATE TABLE `specialists` (
  `userid` int(11) NOT NULL,
  `speciality` int(4) DEFAULT NULL,
  `medregno` varchar(10) DEFAULT NULL,
  `refcode` varchar(6) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `specialists` */

/*Table structure for table `speciality` */

DROP TABLE IF EXISTS `speciality`;

CREATE TABLE `speciality` (
  `id` int(4) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `speciality` */

insert  into `speciality`(`id`,`name`,`created`) values (1,'Allergy & Immunology','2015-10-18 13:31:33'),(2,'Anesthesia','2015-10-18 13:31:44'),(3,'Cardiology','2015-10-18 13:31:53'),(4,'Dermatology','2015-10-18 13:32:00'),(5,'Gastroenterology','2015-10-18 13:32:14');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `mobile` varchar(10) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `type` char(1) DEFAULT NULL,
  `cityid` int(4) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `specialityid` int(4) DEFAULT NULL,
  `medregno` varchar(10) DEFAULT NULL,
  `refcode` varchar(6) DEFAULT NULL,
  `availstart` date DEFAULT NULL,
  `availend` date DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`userid`),
  KEY `city_fk` (`cityid`),
  CONSTRAINT `city_fk` FOREIGN KEY (`cityid`) REFERENCES `cities` (`cityid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `users` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
